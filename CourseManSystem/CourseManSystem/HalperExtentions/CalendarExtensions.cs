﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CourseManSystem.Models;
using Microsoft.Ajax.Utilities;

namespace CourseManSystem.HalperExtentions
{
    public static class CalendarExtensions
    {
        public static IHtmlString Calendar(this HtmlHelper helper, IEnumerable<TopicPlModel> topicModels)
        {
            DateTimeFormatInfo cinfo = DateTimeFormatInfo.CurrentInfo;
            StringBuilder sb = new StringBuilder();
            var topicPlModels = topicModels as IList<TopicPlModel> ?? topicModels.ToList();

            var sortedDays = topicPlModels.OrderBy(o => o.Day).ToArray();
            var firstTopicDate = sortedDays.First().Day;
            var lastTopicDate = sortedDays.Last().Day;
            var monthCount = (lastTopicDate.Year - firstTopicDate.Year) * 12 + (lastTopicDate.Month - firstTopicDate.Month);
            for (var j = 0; j <= monthCount; j++)
            {
                DateTime date = new DateTime(firstTopicDate.AddMonths(j).Year, firstTopicDate.AddMonths(j).Month, 1);
                int emptyCells = ((int)date.DayOfWeek + 7 - (int)cinfo.FirstDayOfWeek) % 7;
                int days = DateTime.DaysInMonth(date.Year, date.Month);
                sb.Append("<div class='table-details'><table class='cal'><tr><th colspan='7'>" + cinfo.MonthNames[date.Month - 1] + " " + date.Year + "</th></tr>");

                for (int i = 0; i < ((days + emptyCells) > 35 ? 42 : 35); i++)
                {
                    if (i % 7 == 0)
                    {
                        if (i > 0) sb.Append("</tr>");
                        sb.Append("<tr>");
                    }

                    if (i < emptyCells || i >= emptyCells + days)
                    {
                        sb.Append("<td class='cal-empty'>&nbsp;</td>");
                    }
                    else
                    {
                        bool isTopicPresent = false;
                        
                        foreach (var topicModel in topicPlModels.DistinctBy(p => p.Day))
                        {
                            if (date == topicModel.Day)
                            {
                                if (topicPlModels.Count(o => o.Day == date) > 1)
                                {
                                    sb.Append($"<td class='cal-day lesson'> {date.Day} <br> ");
                                    foreach (var model in topicPlModels.Where(o => o.Day == date))
                                    {
                                        sb.Append($"<a data-content='{model.Description}' data-placement='bottom' title='' data-toggle='popover' data-original-title='{model.Title}'>{model.Title}</a><br>");
                                    }
                                    sb.Append("</td>");
                                }
                                else
                                {
                                    sb.Append($"<td class='cal-day lesson'> { date.Day} <br> <a data-content='{topicModel.Description}' data-placement='bottom' title='' data-toggle='popover' data-original-title='{topicModel.Title}'>{topicModel.Title}</a> </td>");
                                }
                                
                                isTopicPresent = true;
                            }
                        }
                        if (!isTopicPresent)
                        {
                            sb.Append($"<td class='cal-day'>  {date.Day} <br>  </td>");
                        }
                        date = date.AddDays(1);
                    }
                }
                sb.Append("</tr></table></div>");
            }
            
            return helper.Raw(sb.ToString());
        }
    }
}