﻿using System.Collections.Generic;
using CourseManSystem.BL.Contracts.Models;
using CourseManSystem.Identity.Contracts;
using CourseManSystem.Models;

namespace CourseManSystem.Transformers
{
    public static class DtoTransformer
    {
        public static UserPlModel Convert(IRegisterModel objTotransform)
        {
            return new UserPlModel
            {
                Id = objTotransform.Id,
                Email = objTotransform.Email,
                Password = objTotransform.Password,
                UserName = objTotransform.UserName,
                Role = objTotransform.Role
            };
        }

        public static UserPlModel Convert(IUserBlModel objToTransform)
        {
            return new UserPlModel
            {
                Id = objToTransform.Id,
                Email = objToTransform.Email,
                Password = objToTransform.Password,
                Role = objToTransform.Role,
                UserName = objToTransform.UserName
            };
        }

        public static CoursePlModel Convert(ICourseBlModel objToTransform)
        {

            objToTransform.Topics = objToTransform.Topics ?? new List<ITopicBlModel>();

            return new CoursePlModel
            {
                Id = objToTransform.Id,
                CourseTitle = objToTransform.CourseTitle,
                CreatorName = objToTransform.CreatorName,
                DateOfCreation = objToTransform.DateOfCreation,
                DateOfUpdating = objToTransform.DateOfUpdating,
                Description = objToTransform.Description,
                IsFinished = objToTransform.IsFinished,
                Topics = objToTransform.Topics
            };
        }

        public static TopicPlModel Convert(ITopicBlModel objToTransform)
        {
            return new TopicPlModel
            {
                Id = objToTransform.Id,
                CourseId = objToTransform.CourseId,
                Description = objToTransform.Description,
                Title = objToTransform.Title,
                Day = objToTransform.Day
            };
        }

        public static NotificationPlModel Convert(INotificationBlModel model)
        {
            return new NotificationPlModel
            {
                Id = model.Id,
                Description = model.Description,
                IsViewed = model.IsViewed,
                Title = model.Title,
                UserId = model.UserId
            };
        }
    }
}