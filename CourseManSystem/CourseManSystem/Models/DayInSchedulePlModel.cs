﻿using System;
using System.ComponentModel.DataAnnotations;
using CourseManSystem.BL.Contracts.Models;

namespace CourseManSystem.Models
{
    public class DayInSchedulePlModel : IDayInScheduleBlModel
    {
        public Guid Id { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public bool IsHoliday { get; set; }
    }
}