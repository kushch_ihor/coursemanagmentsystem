﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CourseManSystem.Identity.Contracts;

namespace CourseManSystem.Models
{
    public class LoginModel : ILoginModel
    {
        [Required(ErrorMessage = "You must input your nickname")]
        [StringLength(255)]
        [Display(Prompt = "Your user name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "You must input your password")]
        [DataType(DataType.Password)]
        [StringLength(255, MinimumLength = 6, ErrorMessage = "Your password must be at least at 6 charachters")]
        public string Password { get; set; }
    }
}