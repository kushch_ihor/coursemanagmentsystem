﻿using System;
using System.ComponentModel.DataAnnotations;
using CourseManSystem.BL.Contracts.Models;

namespace CourseManSystem.Models
{
    public class NotificationPlModel : INotificationBlModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "You can't leave this field empty")]
        [StringLength(100, ErrorMessage = "Notification's title must be smaller then 100 symbols")]
        public string Title { get; set; }

        [StringLength(500, ErrorMessage = "Description must have les then 500 symbols")]
        public string Description { get; set; }


        public bool IsViewed { get; set; }

        public Guid UserId { get; set; }
    }
}