﻿using System.ComponentModel.DataAnnotations;

namespace CourseManSystem.Models
{
    public class ResetPasswordModel
    {
        [Required(ErrorMessage = "You must input your current password")]
        [DataType(DataType.Password)]
        [StringLength(255, MinimumLength = 6, ErrorMessage = "Your password must be at least at 6 charachters")]
        [Display(Name = "Current password", Prompt = "Enter your current pass here")]
        public string Password { get; set; }
        
        [DataType(DataType.Password)]
        [Display(Name = "New password", Prompt = "Enter your new pass here")]
        [StringLength(255, MinimumLength = 6, ErrorMessage = "Your password must be at least at 6 charachters")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password", Prompt = "Repeat your new pass here")]
        [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmedNewPassword { get; set; }
    }
}