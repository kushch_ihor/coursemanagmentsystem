﻿using System.ComponentModel.DataAnnotations;

namespace CourseManSystem.Models
{
    public class ExternalRegistrationModel
    {
        [StringLength(255)]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Role")]
        public string Role { get; set; }
    }
}