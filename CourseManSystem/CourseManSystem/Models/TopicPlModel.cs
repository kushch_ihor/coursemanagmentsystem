﻿using System;
using System.ComponentModel.DataAnnotations;
using CourseManSystem.BL.Contracts.Models;

namespace CourseManSystem.Models
{
    public class TopicPlModel : ITopicBlModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "You must set some title to your course's topic")]
        [StringLength(255, ErrorMessage = "Topic title can't be more then 255 symbols")]
        public string Title { get; set; }

        [StringLength(500, ErrorMessage = "Description can't be more then 500 symbols")]
        public string Description { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

        public DateTime Day { get; set; }
        public Guid CourseId { get; set; }
    }
}