﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CourseManSystem.BL.Contracts.Models;
using CourseManSystem.Identity.Contracts;

namespace CourseManSystem.Models
{
    public class UserPlModel : IRegisterModel, IUserBlModel
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(255)]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail format is not valid")]
        public string Email { get; set; }

        [Required(ErrorMessage = "You must input your username")]
        [StringLength(255)]
        public string UserName { get; set; }

        [Display(Name = "Phone Number ")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "You must input your password")]
        [DataType(DataType.Password)]
        [StringLength(255,MinimumLength = 6,ErrorMessage = "Your password must be at least at 6 charachters")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(20)]
        public string Role { get; set; }
        
        public ICollection<ICourseBlModel> Courses { get; set; }
        public ICollection<INotificationBlModel> Notifications { get; set; }
    }
}