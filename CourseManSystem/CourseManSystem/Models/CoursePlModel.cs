﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CourseManSystem.BL.Contracts.Models;

namespace CourseManSystem.Models
{
    public class CoursePlModel : ICourseBlModel
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "You must input title of course")]
        [StringLength(255, ErrorMessage = "Title of course can't be mote ")]
        public string CourseTitle { get; set; }

        [StringLength(1000, ErrorMessage = "Description can't have length more then 1000 symbols")]
        public string Description { get; set; }

        [Required(ErrorMessage = "You must input name of creator")]
        [StringLength(255, ErrorMessage = "Creator name can't be more then 255 symbols")]
        public string CreatorName { get; set; }


        public bool IsFinished { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateOfCreation { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateOfUpdating { get; set; }
        
        public List<IUserBlModel> Members { get; set; }
        public List<ITopicBlModel> Topics { get; set; }

        public CoursePlModel()
        {
            Members = new List<IUserBlModel>();
            Topics = new List<ITopicBlModel>();
        }
    }
}