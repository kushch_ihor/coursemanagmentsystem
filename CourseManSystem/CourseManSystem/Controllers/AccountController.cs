﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using CourseManSystem.BL.Contracts.Servicees;
using CourseManSystem.Identity.Contracts;
using CourseManSystem.Identity.Infrastructure;
using CourseManSystem.Models;
using CourseManSystem.Transformers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;


namespace CourseManSystem.Controllers
{

    [Authorize]
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly IEnumerable<string> _roleList;
        public AccountController(IUserService userService)
        {
            _roleList = Constants.Constants.RolesList.Except(new[] { "admin" });
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult> Manage(string id)
        {
            IRegisterModel foundUser;

            if (id == null)
            {
                foundUser = await UserProvider.FindUserById(Guid.Parse(User.Identity.GetUserId()));
            }
            else
            {
                foundUser = await UserProvider.FindUserById(Guid.Parse(id));
            }
            var list = new List<SelectListItem>();
            if (User.IsInRole("admin"))
            {
                list.Add(new SelectListItem { Text = "admin", Selected = true });
                foreach (var role in _roleList)
                {
                    list.Add(new SelectListItem { Text = role });
                }
            }
            else
            {
                foreach (var role in _roleList)
                {
                    if (foundUser.Role == role)
                    {
                        list.Add(new SelectListItem { Text = role, Selected = true });
                    }
                    else
                    {
                        list.Add(new SelectListItem { Text = role });
                    }
                }
            }
            ViewBag.Roles = list;
            return View(DtoTransformer.Convert(foundUser));
        }

        [HttpPost]
        public async Task<ActionResult> Manage(UserPlModel userToUpdate)
        {
            var result = await UserProvider.UpdateUser(userToUpdate);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Something gone wrong");
            }
            return PartialView("_Confirmation");
        }

        [HttpGet]
        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel model)
        {
            var result = await UserProvider.ResetUserPassword(Guid.Parse(User.Identity.GetUserId()), model.Password, model.NewPassword);
            if (result.Succeeded)
            {
                return PartialView("_Confirmation");
            }
            else
            {
                ModelState.AddModelError("", "Password is not correct");
            }
            return View(model);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<ActionResult> EditAllUsers()
        {
            var userList = await UserProvider.GetAllUsers();
            var userListWithoutAdmin = userList.Where(o => o.Id != Guid.Parse(User.Identity.GetUserId()));
            return View(userListWithoutAdmin.Select(DtoTransformer.Convert).ToList());
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<ActionResult> UserDetails(string id)
        {
            var foundUser = await UserProvider.FindUserById(Guid.Parse(id));
            return PartialView("_UserDetails", DtoTransformer.Convert(foundUser));
        }

        [Authorize(Roles = "admin")]
        public async Task<ActionResult> DeleteUser(string id)
        {

            var result = await UserProvider.DeleteUser(id);
            if (result.Succeeded)
            {
                return PartialView("_Confirmation");
            }
            return HttpNotFound();
        }

        [AllowAnonymous]
        public ActionResult GoogleLogin(string returnUrl)
        {
            var properties = new AuthenticationProperties
            {
                RedirectUri = Url.Action("ExternalLoginCallback", new { returnUrl = returnUrl })
            };
            UserProvider.GetAuthenticationManager.Challenge(properties, "Google");

            return new HttpUnauthorizedResult();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = UserProvider.GetAuthenticationManager.GetExternalLoginInfo();
            var userIsPresent = await UserProvider.FindUserAsync(loginInfo.Login);
            if (userIsPresent)
            {
                var ident = await UserProvider.GetUserManager().CtreateIdentityForExternalServise(loginInfo.Login);

                UserProvider.GetAuthenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = false
                }
                    , ident);
                return RedirectToAction("Manage");
            }
            ViewBag.ReturnUrl = returnUrl;
            ViewBag.Roles = _roleList;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginCallback(ExternalRegistrationModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            ViewBag.Roles = _roleList;
            ViewBag.ReturnUrl = returnUrl;

            var loginInfo = UserProvider.GetAuthenticationManager.GetExternalLoginInfo();
            var userIsPresent = await UserProvider.FindUserAsync(loginInfo.Login);

            if (!userIsPresent)
            {
                var user = new UserPlModel
                {
                    Email = loginInfo.Email,
                    UserName = loginInfo.DefaultUserName,
                    Role = model.Role
                };

                if (model.UserName != null)
                {
                    user.UserName = model.UserName;
                }

                var result = await UserProvider.CreateNewUser(user);

                if (!result.Succeeded)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                    return View("ExternalLoginCallback", model);
                }
                else
                {
                    result = await UserProvider.AddLoginAsync(user.UserName, loginInfo.Login);
                    if (!result.Succeeded)
                    {
                        foreach (string error in result.Errors)
                        {
                            ModelState.AddModelError("", error);
                        }
                        return View("ExternalLoginCallback", model);
                    }
                }
                var ident = await UserProvider.GetUserManager().CtreateIdentityForExternalServise(loginInfo.Login);

                UserProvider.GetAuthenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = false
                }
                    , ident);
                return RedirectToAction("Manage");
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult FacebookLogin(string returnUrl)
        {
            var properties = new AuthenticationProperties
            {
                RedirectUri = Url.Action("ExternalLoginCallback", new { returnUrl = returnUrl })
            };
            UserProvider.GetAuthenticationManager.Challenge(properties, "Facebook");
            return new HttpUnauthorizedResult();
        }


        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            ViewBag.Roles = _roleList;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(UserPlModel objToSignIn)
        {
            if (ModelState.IsValid)
            {
                IdentityResult result = UserProvider.CreateNewUser(objToSignIn, objToSignIn.Password);
                if (result.Succeeded)
                {
                    
                    return View("Congratulation");
                }
                foreach (string error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }
            }
            return View(objToSignIn);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult SignIn()
        {
            ViewBag.returnUrl = Url.Action("Manage", "Account");
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SignIn(LoginModel objToSignIn)
        {
            if (ModelState.IsValid)
            {
                var userIsPresent = await UserProvider.FindUserAsync(objToSignIn.UserName, objToSignIn.Password);

                if (!userIsPresent)
                {
                    ModelState.AddModelError("", "Username or password is incorrect");
                }
                else
                {
                    ClaimsIdentity ident = await UserProvider.GetUserManager().CreateIdentityForLoginModelAsync(objToSignIn);
                    var authenticationManager = UserProvider.GetAuthenticationManager;
                    authenticationManager.SignOut();
                    authenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, ident);

                    Guid userId = Guid.Parse(ident.GetUserId());

                    return RedirectToAction("AssignedCourses", "Course", new { id = userId });
                }
            }

            return View(objToSignIn);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            UserProvider.GetAuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

    }
}