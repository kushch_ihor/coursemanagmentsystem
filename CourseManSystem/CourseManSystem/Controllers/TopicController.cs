﻿using System;
using System.Web.Mvc;
using CourseManSystem.BL.Contracts.Servicees;
using CourseManSystem.Models;
using CourseManSystem.Transformers;

namespace CourseManSystem.Controllers
{
    [Authorize]
    public class TopicController : Controller
    {
        private readonly ITopicService _topicService;
        private readonly ICourseService _courseService;

        public TopicController(ITopicService topicService, ICourseService courseService)
        {
            _topicService = topicService;
            _courseService = courseService;
        }

        [HttpGet]
        public ActionResult TopicList(string id)
        {
            if (id == null || Guid.Parse(id)  == Guid.Empty)
            {
                return View("Error");
            }
            var foundCourse = _courseService.GetOneById(Guid.Parse(id));

            return View(DtoTransformer.Convert(foundCourse));
        }

        [Authorize(Roles = "admin, trainer" )]
        [HttpGet]
        public ActionResult Update(Guid? id)
        {
            if (id == null)
            {
                return View("Error");
            }
            var topic = _topicService.GetOneById(id.Value);
            return PartialView(DtoTransformer.Convert(topic));
        }

        [HttpPost]
        public ActionResult Update(TopicPlModel model)
        {
            if (ModelState.IsValid)
            {
               var success =  _topicService.Update(model);
                if (success)
                {
                    return RedirectToAction("TopicList", new { id = model.CourseId.ToString() });
                }
            }
            return View("Error");
        }


        [HttpGet]
        public ActionResult Create(string id)
        {
            ViewBag.id = id;
            return View();
        }

        [HttpPost]
        public ActionResult Create(TopicPlModel model, string id)
        {
            if (ModelState.IsValid && id != null)
            {
                model.CourseId = Guid.Parse(id);
                var successed = _topicService.Create(model);
                if (successed)
                {
                    return RedirectToAction("TopicList", new { id = id });
                }
            }
            return View("Error");
        }

        [HttpGet]
        public ActionResult Delete(string id, string courseId)
        {
            if (id == null || courseId == null)
            {
                return View("Error");
            }
            var successed =  _topicService.Delete(Guid.Parse(id));
            if (successed)
            {
                return RedirectToAction("TopicList", new { id = courseId });
            }
            return View("Error");
        }
        
    }
}