﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using CourseManSystem.BL.Contracts.Servicees;
using CourseManSystem.Identity.Infrastructure;
using CourseManSystem.Models;
using CourseManSystem.Transformers;
using Microsoft.AspNet.Identity;
using WebGrease.Css.Extensions;

namespace CourseManSystem.Controllers
{
    [Authorize]
    public class CourseController : Controller
    {
        private readonly ICourseService _courseService;
        private readonly IUserService _userService;

        public CourseController(ICourseService courseService, IUserService userService, ITopicService topicService)
        {
            _userService = userService;
            _courseService = courseService;
        }

        // GET: Course
        [Authorize(Roles = "admin")]
        public ActionResult Index()
        {
            var courseList = _courseService.GetAll().Select(o => DtoTransformer.Convert(o)).ToList();
            return View(courseList);
        }

        [HttpGet]
        public ActionResult AssignedCourses(string id)
        {
            if (id == null)
            {
                return View("Error");
            }
            List<CoursePlModel> courseList = _userService.GetOneById(Guid.Parse(id))
                                             .Courses.Select(o => DtoTransformer.Convert(o)).ToList();
            return View(courseList);
        }

        [HttpGet]

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CoursePlModel objToCreate)
        {
            if (ModelState.IsValid)
            {
                objToCreate.Members.Add(new UserPlModel{Id = Guid.Parse(User.Identity.GetUserId())});
                var courseId = _courseService.Create(objToCreate);
                if (courseId == null)
                {
                    return View("Error");
                }
                objToCreate.Id = courseId.Value;
            }
            return RedirectToAction("TopicList","Topic", new {id = objToCreate.Id.ToString()});
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return View("Error");
            }
            var course = _courseService.GetOneById(Guid.Parse(id));
            return View(DtoTransformer.Convert(course));
        }

        [HttpPost]
        public ActionResult Edit(CoursePlModel model)
        {
            if (ModelState.IsValid)
            {
                var succeed = _courseService.Update(model);
                if (succeed)
                {
                    return View(model);
                }
            }
            return View("Error");
        }

        //https://antiyes.com/2015/05/26/asp-net-mvc-calendar-helper/
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Details(Guid? id, bool isUserId)
        {
            if (id == null)
            {
                return View("Error");
            }

            var topicList = new List<TopicPlModel>
            {
                //new TopicPlModel{Day = DateTime.Today, Title = "mathematics",Description = "mathematics description"},
                //new TopicPlModel{Day = DateTime.Today, Title = "history", Description = "History Description"},
                //new TopicPlModel{Day = DateTime.Today.AddDays(96), Title = "Geography"},
                //new TopicPlModel{Day = DateTime.Today.AddDays(40), Title = "Literature"},
                //new TopicPlModel{Day = DateTime.Today.AddDays(600), Title = "English"}
            };

            if (isUserId)
            {
               var userProfile =  _userService.GetOneById(id.Value);
               userProfile.Courses.Select(DtoTransformer.Convert).ForEach(o =>
               {
                   foreach (var topicPlModel in o.Topics.Select(DtoTransformer.Convert))
                       topicList.Add(topicPlModel);
               });
            }
            else
            {
               var foundCourse =  _courseService.GetOneById(id.Value);
               topicList = foundCourse.Topics.Select(DtoTransformer.Convert).ToList();
            }

            return View(topicList);
        }

        public ActionResult Unsubscribe(Guid courseId, Guid userId)
        {
            _courseService.RemoveUserFromCourse(courseId, userId);
            return RedirectToAction("Members", new {id = courseId});
        }

        [HttpGet]
        public ActionResult Subscribe(Guid courseId, Guid userId)
        {
            if (courseId != Guid.Empty || userId != Guid.Empty)
            {
                var succeed = _courseService.AddUserToCourse(courseId, userId);
                if (succeed)
                {
                    return RedirectToAction("NonMembers", new { id = courseId });
                }    
            }
            return View("Error");
        }

        public async Task<ActionResult> NonMembers(Guid id)
        {
            if (id != Guid.Empty)
            {
                ViewBag.courseId = id;
                var members = _courseService
                            .GetOneById(id)
                            .Members.ToList()
                            .Select(o => DtoTransformer.Convert(o))
                            .ToList();
                var userList = await UserProvider.GetAllUsers();
                var nonMembers = userList
                            .Where(o => o.Role == "listener")
                            .ToList()
                            .Where(o => !members.Select(t => t.Id)
                            .ToList()
                            .Contains(o.Id));
                return View(nonMembers.Select(DtoTransformer.Convert));
            }
            return View("Error");
        }

        [HttpGet]
        public async Task<ActionResult> Members(Guid id)
        {
            if (id != Guid.Empty)
            {
                ViewBag.courseId = id;
                var members = _courseService.GetOneById(id).Members.ToList().
                    Select(o => DtoTransformer.Convert(o)).ToList();

                var userList = await UserProvider.GetAllUsers();
                var listeners = userList.Where(o => o.Role == "listener").ToList()
                    .Where(o => members.Select(t => t.Id).ToList().Contains(o.Id));

                return View(listeners.Select(o => DtoTransformer.Convert(o)).ToList());
            }
            return View("Error");
        }

        [HttpGet]
        public ActionResult Delete(Guid id)
        {
            if (id != Guid.Empty)
            {
                var isDeleted = _courseService.Delete(id);
                if (isDeleted)
                {
                    return RedirectToAction("Index");
                }
            }
            return View("Error");
        }
    }
}