﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CourseManSystem.BL.Contracts.Servicees;
using CourseManSystem.Models;
using CourseManSystem.Transformers;

namespace CourseManSystem.Controllers
{
    [Authorize]
    public class NotificationController : Controller
    {
        private readonly INotificationService _notificationService;

        public NotificationController(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        [Authorize(Roles = "admin")]
        public ActionResult Index()
        {
            var messageList = _notificationService.GetAll();

            return View((messageList.Select(DtoTransformer.Convert).ToList()));
        }

        [HttpGet]
        public ActionResult ShowNotification(string id)
        {
            IEnumerable<NotificationPlModel> messageList = new List<NotificationPlModel>();
             messageList = _notificationService.GetAllByFunc(o => o.UserId == Guid.Parse(id)).Select(DtoTransformer.Convert);
            return View(messageList);
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Create(string userId)
        {
            if (userId != null)
            {
                ViewBag.userId = Guid.Parse(userId);
            }
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Create([Bind(Exclude = "Id")]NotificationPlModel model)
        {
            if (ModelState.IsValid)
            {
                var isSuccess = _notificationService.Create(model);
                if (isSuccess)
                {
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "Something gone wrong");
            }
            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Delete(string id)
        {
            if (id != null)
            {
                var isDeleted = _notificationService.Delete(Guid.Parse(id));
                if (isDeleted)
                {
                    return RedirectToAction("Index");
                }
            }
            return View("Error");
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Edit(string id)
        {
            if (id != null)
            {
                var notification =  _notificationService.GetOneById(Guid.Parse(id));
                if (notification != null)
                {
                    return View(DtoTransformer.Convert(notification));
                }
            }
            return View("Error");
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Edit(NotificationPlModel model)
        {
            if (ModelState.IsValid)
            {
                var isUpdated = _notificationService.Update(DtoTransformer.Convert(model));
                if (isUpdated)
                {
                    return RedirectToAction("Index");
                }
            }
            return View("Error");
        }
    }
}