﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CourseManSystem.BL.Contracts.Servicees;
using CourseManSystem.BL.Services;
using Ninject;

namespace CourseManSystem
{
    public class NinjecttDependencyResolver : IDependencyResolver
    {
        private readonly IKernel _ninjectKernel;

        public NinjecttDependencyResolver()
        {
            _ninjectKernel = new StandardKernel();
            AddBindings();
        }

        private void AddBindings()
        {
            _ninjectKernel.Bind<INotificationService>().To<NotificationService>();
            _ninjectKernel.Bind<ICourseService>().To<CourseService>();
            _ninjectKernel.Bind<IDayInScheduleService>().To<DayInScheduleService>();
            _ninjectKernel.Bind<ITopicService>().To<TopicService>();
            _ninjectKernel.Bind<IUserService>().To<UserService>();
        }

        public object GetService(Type serviceType)
        {
            return _ninjectKernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _ninjectKernel.GetAll(serviceType);
        }
    }
}