﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseManSystem.DAL.Contracts.Dtos;
using CourseManSystem.DAL.Contracts.Repositories;
using CourseManSystem.DAL.Entities.Entity;
using CourseManSystem.DAL.Helpers;

namespace CourseManSystem.DAL.Repositories
{
    public class DayInScheduleRepo : IDayInScheduleRepo
    {
        public bool Create(IDayInScheduleDataDto objToCreate)
        {
            using (var context = new CourseContext())
            {
                context.DaysInSchedule.Add(DtoTransformer.Convert(objToCreate));
                return context.SaveChanges() != 0;
            }
        }

        public bool Delete(Guid idObjectToDel)
        {
            using (var context = new CourseContext())
            {
                var foundObj = context.DaysInSchedule.FirstOrDefault(o => o.Id == idObjectToDel);
                if (foundObj != null)
                {
                    context.DaysInSchedule.Remove(foundObj);
                    return context.SaveChanges() != 0;
                }
                return false;
            }
        }

        public bool Update(IDayInScheduleDataDto objToUpdate)
        {
            using (var context = new CourseContext())
            {
                var foundObj = context.DaysInSchedule.FirstOrDefault(o => o.Id == objToUpdate.Id);
                if (foundObj != null)
                {
                    foundObj.IsHoliday = objToUpdate.IsHoliday;
                    foundObj.Date = objToUpdate.Date;
                    return context.SaveChanges() != 0;
                }
                return false;
            }
        }

        public IDayInScheduleDataDto GetOneByFunc(Func<IDayInScheduleDataDto, bool> predicate)
        {
            using (var context = new CourseContext())
            {
                return context
                    .DaysInSchedule
                    .Select(o => DtoTransformer.Convert(o))
                    .Where(predicate)
                    .FirstOrDefault();
            }
        }

        public ICollection<IDayInScheduleDataDto> GetAll()
        {
            using (var context = new CourseContext())
            {
                return context.DaysInSchedule.Select(o => DtoTransformer.Convert(o)).ToList(); ;
            }
        }

        public ICollection<IDayInScheduleDataDto> GetAllByFunc(Func<IDayInScheduleDataDto, bool> predicate)
        {
            using (var context = new CourseContext())
            {
                return context.DaysInSchedule.Select(o => DtoTransformer.Convert(o)).Where(predicate).ToList(); ;
            }
        }
        
    }
}
