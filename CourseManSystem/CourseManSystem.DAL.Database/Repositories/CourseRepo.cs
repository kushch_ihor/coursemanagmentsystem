﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseManSystem.DAL.Contracts.Dtos;
using CourseManSystem.DAL.Contracts.Repositories;
using CourseManSystem.DAL.Entities.Entity;
using CourseManSystem.DAL.Helpers;

namespace CourseManSystem.DAL.Repositories
{
    public class CourseRepo : ICourseRepo
    {
        public Guid? Create(ICourseDataDto objTocreate)
        {
            using (var context = new CourseContext())
            {
                var course = context.Courses.Add(DtoTransformer.Convert(objTocreate));
                foreach (var member in objTocreate.Members)
                {
                    course.UserProfiles.Add(context.UserProfiles.Find(member.Id));
                }
                if (context.SaveChanges() != 0)
                {
                    return course.Id;
                }
                return null;
            }
        }

        public bool Delete(Guid idObjextToDel)
        {
            using (var context = new CourseContext())
            {
                var foundObj = context.Courses.FirstOrDefault(o => o.Id == idObjextToDel);
                if (foundObj != null)
                {
                    context.Courses.Remove(foundObj);
                    return context.SaveChanges() != 0;
                }
                return false;
            }
        }

        public bool Update(ICourseDataDto objToUpdate)
        {
            using (var context = new CourseContext())
            {
                var foundOb = context.Courses.FirstOrDefault(o => o.Id == objToUpdate.Id);
                if (foundOb != null)
                {
                    foundOb.IsFinished = objToUpdate.IsFinished;
                    foundOb.CourseTitle = objToUpdate.CourseTitle;
                    foundOb.CreatorName = objToUpdate.CreatorName;
                    foundOb.DateofUpdating = DateTime.Now;
                    foundOb.Description = objToUpdate.Description;
                    foundOb.Topics = DtoTransformer.Convert(objToUpdate.Topics);
                    foundOb.UserProfiles = DtoTransformer.Convert(objToUpdate.Members);
                    return context.SaveChanges() != 0;
                }
                return false;
            }
        }

        public bool AddUserToCourse(Guid courseId, Guid userId)
        {
            using (var context = new CourseContext())
            {
                var course = context.Courses.Find(courseId);
                var user = context.UserProfiles.Find(userId);
                if (user != null)
                {
                    course?.UserProfiles.Add(user);
                }
                return context.SaveChanges() != 0;
            }
        }

        public bool RemoveUserFromCourse(Guid courseId, Guid userId)
        {
            using (var context = new CourseContext())
            {
                var course = context.Courses.Find(courseId);
                var user = context.UserProfiles.Find(userId);
                if (user != null)
                {
                    course?.UserProfiles.Remove(user);
                }
                return context.SaveChanges() != 0;
            }
        }


        public ICourseDataDto GetOneByFunc(Func<ICourseDataDto, bool> predicate)
        {
            using (var context = new CourseContext())
            {
                
                var course = context.Courses.ToList().Select(o => DtoTransformer.Convert(o)).
                    ToList().Where(predicate).FirstOrDefault();

                return course;
            }
        }

        public ICollection<ICourseDataDto> GetAll()
        {
            using (var context = new CourseContext())
            {
                return context.Courses.ToList().Select(o => DtoTransformer.Convert(o)).ToList();
            }
        }

        public ICollection<ICourseDataDto> GetAllByFunc(Func<ICourseDataDto, bool> predicate)
        {
            using (var context = new CourseContext())
            {
                return context.Courses.Select(o => DtoTransformer.Convert(o)).Where(predicate).ToList();
            }
        }

    }
}
