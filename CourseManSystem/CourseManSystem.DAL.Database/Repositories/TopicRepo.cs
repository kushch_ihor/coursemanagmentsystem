﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseManSystem.DAL.Contracts.Dtos;
using CourseManSystem.DAL.Contracts.Repositories;
using CourseManSystem.DAL.Entities.Entity;
using CourseManSystem.DAL.Helpers;

namespace CourseManSystem.DAL.Repositories
{
    public class TopicRepo : ITopicRepo
    {
        public bool Create(ITopicDataDto objToCreate)
        {
            using (var context = new CourseContext())
            {
                var course = context.Courses.Find(objToCreate.CourseId);
                course?.Topics.Add(DtoTransformer.Convert(objToCreate));
                return context.SaveChanges() != 0;
            }
        }

        public bool Delete(Guid idObjextToDel)
        {
            using (var context = new CourseContext())
            {
                var foundObj = context.Topics.FirstOrDefault(o => o.Id == idObjextToDel);
                if (foundObj != null)
                {
                    context.Topics.Remove(foundObj);
                    return context.SaveChanges() != 0;
                }
                return false;
            }
        }

        public bool Update(ITopicDataDto objToUpdate)
        {
            using (var context = new CourseContext())
            {
                var foundObj = context.Topics.FirstOrDefault(o => o.Id == objToUpdate.Id);
                if (foundObj != null)
                {
                    foundObj.Title = objToUpdate.Title;
                    foundObj.Description = objToUpdate.Description;
                    var foundDay = context.DaysInSchedule.SingleOrDefault(o => o.Date == objToUpdate.Day);
                    if (foundDay != null)
                    {
                        foundObj.DaysInSchedule.Add(foundDay);
                    }
                    else
                    {
                        foundObj.DaysInSchedule.Add(new DayInSchedule
                        {
                            Date = objToUpdate.Day,
                            IsHoliday = false
                        });
                    }
                    return context.SaveChanges() != 0;
                }
                return false;
            }
        }

        public ITopicDataDto GetOneByFunc(Func<ITopicDataDto, bool> predicate)
        {
            using (var context = new CourseContext())
            {
                return context
                    .Topics
                    .ToList()
                    .Select(o => DtoTransformer.Convert(o))
                    .FirstOrDefault(predicate);
            }
        }

        public ICollection<ITopicDataDto> GetAll()
        {
            using (var context = new CourseContext())
            {
                return context.Topics.Select(o => DtoTransformer.Convert(o)).ToList();
            }
        }

        public ICollection<ITopicDataDto> GetAllByFunc(Func<ITopicDataDto, bool> predicate)
        {
            using (var context = new CourseContext())
            {
                return context.Topics.Select(o => DtoTransformer.Convert(o)).Where(predicate).ToList();
            }
        }
        
    }
}
