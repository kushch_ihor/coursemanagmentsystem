﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseManSystem.DAL.Contracts.Dtos;
using CourseManSystem.DAL.Contracts.Repositories;
using CourseManSystem.DAL.Entities.Entity;
using CourseManSystem.DAL.Helpers;

namespace CourseManSystem.DAL.Repositories
{
    public class NotificationRepo : INotificationRepo
    {
        public bool Create(INotificationDataDto objTocreate)
        {
            using (var context = new CourseContext())
            {
                var foundUser = context.UserProfiles.Find(objTocreate.UserId);
                if (foundUser != null)
                {
                    foundUser.Notifications.Add(DtoTransformer.Convert(objTocreate));
                }
                return context.SaveChanges() != 0;
            }
        }

        public bool Delete(Guid idObjextToDel)
        {
            using (var context = new CourseContext())
            {
                var foundObj = context.Notifications.FirstOrDefault(o => o.Id == idObjextToDel);
                if (foundObj != null)
                {
                    context.Notifications.Remove(foundObj);
                    return context.SaveChanges() != 0;
                }
                return false;
            }
        }

        public bool Update(INotificationDataDto objToUpdate)
        {
            using (var context = new CourseContext())
            {
                var foundObj = context.Notifications.FirstOrDefault(o => o.Id == objToUpdate.Id);
                if (foundObj != null)
                {
                    foundObj.Title = objToUpdate.Title;
                    foundObj.Description = objToUpdate.Description;
                    return context.SaveChanges() != 0;
                }
                return false;
            }
        }

        public INotificationDataDto GetOneByFunc(Func<INotificationDataDto, bool> predicate)
        {
            using (var context = new CourseContext())
            {
                return context.Notifications.ToList().Select(o => DtoTransformer.Convert(o)).FirstOrDefault(predicate);
            }
        }

        public ICollection<INotificationDataDto> GetAll()
        {
            using (var context = new CourseContext())
            {
                if (!context.Notifications.Any())
                {
                    return new List<INotificationDataDto>();
                }
                return context.Notifications.ToList().Select(o => DtoTransformer.Convert(o)).ToList();
            }
        }

        public ICollection<INotificationDataDto> GetAllByFunc(Func<INotificationDataDto, bool> predicate)
        {
            using (var context = new CourseContext())
            {
                return context.Notifications.ToList().Select(o => DtoTransformer.Convert(o)).Where(predicate).ToList();
            }
        }
        
    }
}
