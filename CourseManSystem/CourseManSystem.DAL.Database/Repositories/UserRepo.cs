﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseManSystem.DAL.Contracts.Dtos;
using CourseManSystem.DAL.Contracts.Repositories;
using CourseManSystem.DAL.Entities.Entity;
using CourseManSystem.DAL.Helpers;

namespace CourseManSystem.DAL.Repositories
{
    public class UserRepo : IUserRepo
    {

        public bool Create(IUserDataDto objToCreate)
        {
            using (var context = new CourseContext())
            {
               var checkUser = context.UserProfiles.Add(DtoTransformer.Convert(objToCreate));
                if (checkUser != null)
                {
                    return context.SaveChanges() != 0;
                }
            }
            return false;
        }

        public bool Delete(Guid idObjectToDel)
        {
            using (var context = new CourseContext())
            {
                var obToDel = context.UserProfiles.FirstOrDefault(o => o.Id == idObjectToDel);
                if (obToDel != null)
                {
                    context.UserProfiles.Remove(obToDel);
                    return context.SaveChanges() != 0;
                }
                return false;
            }
        }

        public bool Update(IUserDataDto objToUpdate)
        {
            using (var context = new CourseContext())
            {
                var findedOb = context.UserProfiles.FirstOrDefault(o => o.Id == objToUpdate.Id);
                if (findedOb != null)
                {
                    return context.SaveChanges() != 0;
                }
                return false;
            }
        }

        public bool AssignCourseToUser(Guid userId, Guid courseId)
        {
            using (var context = new CourseContext())
            {
                var foundCourse =  context.Courses.SingleOrDefault(o => o.Id == courseId);
                if (foundCourse != null)
                {
                    var foundUser = context.UserProfiles.SingleOrDefault(o => o.Id == userId);
                    //foundUser.Courses.Add(foundCourse);
                    return context.SaveChanges() != 0;
                }
                return false;
            }
        }
        public IUserDataDto GetOneByFunc(Func<IUserDataDto, bool> predicate)
        {
            using (var context = new CourseContext())
            {
                var userList = context.UserProfiles.ToList();
                var user = userList.Select(o => DtoTransformer.Convert(o)).FirstOrDefault(predicate);
                return user;
            }
        }

        public ICollection<IUserDataDto> GetAll()
        {
            using (var context = new CourseContext())
            {
                var userList = context.UserProfiles.ToList();
                return userList.Select(o => DtoTransformer.Convert(o)).ToList();
            }
        }

        public ICollection<IUserDataDto> GetAllByFunc(Func<IUserDataDto, bool> predicate)
        {
            using (var context = new CourseContext())
            {
                return context.UserProfiles.Select(o => DtoTransformer.Convert(o)).Where(predicate).ToList();
            }
        }

    }
}
