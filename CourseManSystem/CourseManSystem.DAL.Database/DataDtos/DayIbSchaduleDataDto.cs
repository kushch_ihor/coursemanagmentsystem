﻿using System;
using CourseManSystem.DAL.Contracts.Dtos;

namespace CourseManSystem.DAL.DataDtos
{
    public class DayIbSchaduleDataDto : IDayInScheduleDataDto
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public bool IsHoliday { get; set; }
    }
}
