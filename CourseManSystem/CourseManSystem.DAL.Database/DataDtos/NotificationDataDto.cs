﻿using System;
using CourseManSystem.DAL.Contracts.Dtos;

namespace CourseManSystem.DAL.DataDtos
{
    public class NotificationDataDto : INotificationDataDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsViewed { get; set; }
        public Guid UserId { get; set; }
    }
}
