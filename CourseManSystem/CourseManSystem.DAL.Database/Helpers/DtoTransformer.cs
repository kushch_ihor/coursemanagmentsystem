﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseManSystem.DAL.Contracts.Dtos;
using CourseManSystem.DAL.DataDtos;
using CourseManSystem.DAL.Entities.Entity;

namespace CourseManSystem.DAL.Helpers
{
    public static class DtoTransformer
    {
        #region Course Converters
        /// <summary>
        /// Transforms database object to dto object
        /// </summary>
        /// <param name="objToTransform">Course Database object</param>
        /// <returns>CourseDTO</returns>
        public static ICourseDataDto Convert(Course objToTransform)
        {
            return new CourseDataDto
            {
                CourseTitle = objToTransform.CourseTitle,
                CreatorName = objToTransform.CreatorName,
                DateOfUpdating = objToTransform.DateofUpdating,
                DateOfCreation = objToTransform.DateOfCreation,
                Description = objToTransform.Description,
                Id = objToTransform.Id,
                IsFinished = objToTransform.IsFinished,
                Members = Convert(objToTransform.UserProfiles),
                Topics = Convert(objToTransform.Topics)
            };
        }
        /// <summary>
        /// Transforms dto object to database object
        /// </summary>
        /// <param name="objToEntity">Course dto</param>
        /// <returns>Database Course object</returns>
        public static Course Convert(ICourseDataDto objToEntity)
        {

            return new Course
            {
                CourseTitle = objToEntity.CourseTitle,
                CreatorName = objToEntity.CreatorName,
                DateofUpdating = DateTime.Now,
                DateOfCreation = DateTime.Now,
                Description = objToEntity.Description,
                Id = objToEntity.Id,
                IsFinished = objToEntity.IsFinished,
                Topics = new List<Topic>(),
                UserProfiles = new List<UserProfile>()
                //Topics = TopicListToDtoList(objToEntity.Topics),
                //UserProfiles = EntityListToDtoList(objToEntity.Members)
            };
        }
        #endregion

        #region Course List Converters
        /// <summary>
        /// Transforms List of Course to CourseDTO List
        /// </summary>
        /// <param name="courseList">ICollection<Course></param>
        /// <returns>ICollection<ICourseDataDto></returns>
        public static ICollection<ICourseDataDto> Convert(ICollection<Course> courseList)
        {
            ICollection<ICourseDataDto> courseDtos = new List<ICourseDataDto>();
            foreach (var course in courseList)
            {
                courseDtos.Add(new CourseDataDto
                {
                    Id = course.Id,
                    CourseTitle = course.CourseTitle,
                    CreatorName = course.CreatorName,
                    Description = course.Description,
                    IsFinished = course.IsFinished,
                    DateOfCreation = course.DateOfCreation,
                    DateOfUpdating = course.DateofUpdating,
                    Topics = course.Topics.Select(Convert).ToList()
                });
            }
            return courseDtos;
        }


        #endregion

        #region UserProfile Converters
        /// <summary>
        /// Transforms user database object to user DTO
        /// </summary>
        /// <param name="obToDto">UserProfile object</param>
        /// <returns>user DTO</returns>
        public static IUserDataDto Convert(UserProfile obToDto)
        {
            return new UserDataDto
            {
                Id = obToDto.Id,
                Notifications = Convert(obToDto.Notifications),
                Courses = Convert(obToDto.Courses)
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obToDto">User DTO</param>
        /// <returns>User Database instance</returns>
        public static UserProfile Convert(IUserDataDto obToDto)
        {
            return new UserProfile
            {

            };
        }

        #endregion

        #region UserProfiles List Converters
        /// <summary>
        /// Transforms Userprofiles DTOs list To User Database objects list
        /// </summary>
        /// <param name="dtoList">List of user DTOs</param>
        /// <returns>List of Userprofiles objects</returns>
        public static ICollection<UserProfile> Convert(ICollection<IUserDataDto> dtoList)
        {
            List<UserProfile> userList = new List<UserProfile>();
            foreach (var dto in dtoList)
            {
                userList.Add(new UserProfile
                {
                    Id = dto.Id
                });
            }

            return userList;
        }
        /// <summary>
        /// Transforms UserRofiles list to dto list
        /// </summary>
        /// <param name="userList">List of Userprofiles objects</param>
        /// <returns>List of user DTOs</returns>
        public static ICollection<IUserDataDto> Convert(ICollection<UserProfile> userList)
        {
            ICollection<IUserDataDto> users = new List<IUserDataDto>();
            foreach (var user in userList)
            {
                users.Add(new UserDataDto
                {
                    Id = user.Id
                });
            }
            return users;
        }
        #endregion

        #region Topic Converter
        /// <summary>
        /// Transforms Topic to Topic DTO
        /// </summary>
        /// <param name="objToDto">Topic instance</param>
        /// <returns>Topic DTO</returns>
        public static ITopicDataDto Convert(Topic objToDto)
        {
            return new TopicDataDto
            {
                Description = objToDto.Description,
                Id = objToDto.Id,
                Title = objToDto.Title,
                CourseId = objToDto.CourseId,
                Day = objToDto.DaysInSchedule.FirstOrDefault().Date
            };
        }
        /// <summary>
        /// Transforms Topic DTO to Topic database object
        /// </summary>
        /// <param name="objToEntity">Topic DTO</param>
        /// <returns>Topic instance</returns>
        public static Topic Convert(ITopicDataDto objToEntity)
        {
            return new Topic
            {
                Description = objToEntity.Description,
                Title = objToEntity.Title,
                Id = objToEntity.Id,
                CourseId = objToEntity.CourseId
            };
        }
        #endregion

        #region Topics List Converters
        /// <summary>
        /// Transforms Topic list to Topic DTO list
        /// </summary>
        /// <param name="objList">Topic instance list</param>
        /// <returns>Topic DTO list</returns>
        public static ICollection<ITopicDataDto> Convert(ICollection<Topic> objList)
        {
            ICollection<ITopicDataDto> topics = new List<ITopicDataDto>();
            foreach (var topic in objList)
            {
                if (topic.DaysInSchedule != null && topic.DaysInSchedule.Count != 0)
                {
                    topics.Add(new TopicDataDto
                    {
                        Title = topic.Title,
                        CourseId = topic.CourseId,
                        Description = topic.Description,
                        Id = topic.Id,
                        Day = topic.DaysInSchedule.First().Date
                    });
                }
                else
                {
                    topics.Add(new TopicDataDto
                    {
                        Title = topic.Title,
                        CourseId = topic.CourseId,
                        Description = topic.Description,
                        Id = topic.Id
                    });
                }
                
            }

            return topics;
        }
        /// <summary>
        /// Transforms Topic DTO list to Topic database object list
        /// </summary>
        /// <param name="dtoList">Topic DTO list</param>
        /// <returns>Topic instance list</returns>
        public static ICollection<Topic> Convert(ICollection<ITopicDataDto> dtoList)
        {
            return dtoList?.Select(o => new Topic
            {
                CourseId = o.CourseId,
                Description = o.Description,
                Title = o.Title
            }).ToList();
        }
        #endregion

        #region Notification Converters
        /// <summary>
        /// Transforms Notification To Notification DTO
        /// </summary>
        /// <param name="objToDto">Notification object</param>
        /// <returns>Notification DTO</returns>
        public static INotificationDataDto Convert(Notification objToDto)
        {
            return new NotificationDataDto
            {
                Description = objToDto.Description,
                Id = objToDto.Id,
                Title = objToDto.Title,
                IsViewed = objToDto.IsViewed,
                UserId = objToDto.UserProfileId
            };
        }
        /// <summary>
        /// Transforms Notification DTO To Notification
        /// </summary>
        /// <param name="objToEntity"> Notification DTO </param>
        /// <returns>Notification object</returns>
        public static Notification Convert(INotificationDataDto objToEntity)
        {
            return new Notification
            {
                Description = objToEntity.Description,
                Id = objToEntity.Id,
                Title = objToEntity.Title,
                IsViewed = objToEntity.IsViewed,
                UserProfileId = objToEntity.UserId
            };
        }

        #endregion

        #region Notification List Converters
        /// <summary>
        /// Transforms List of Notifications to Notification DTos list
        /// </summary>
        /// <param name="notifications">Notification list</param>
        /// <returns>Dto list</returns>
        public static ICollection<INotificationDataDto> Convert(ICollection<Notification> notifications)
        {

            ICollection<INotificationDataDto> messageList = new List<INotificationDataDto>();

            foreach (var notification in notifications)
            {
                messageList.Add(new NotificationDataDto
                {
                    Description = notification.Description,
                    Id = notification.Id,
                    IsViewed = notification.IsViewed,
                    Title = notification.Title
                });
            }
            return messageList;
        }
        
        #endregion

        #region DayInSchedule Converters
        /// <summary>
        /// Transforms DayInSchedule to DayInSchedule DTO
        /// </summary>
        /// <param name="obToDto">Day in Schedule object</param>
        /// <returns>DTO of DayInSchedule</returns>
        public static IDayInScheduleDataDto Convert(DayInSchedule obToDto)
        {
            return new DayIbSchaduleDataDto
            {
                Date = obToDto.Date,
                Id = obToDto.Id,
                IsHoliday = obToDto.IsHoliday
            };
        }
        /// <summary>
        /// Transforms DayInSchedule DTO to DayInSchedule 
        /// </summary>
        /// <param name="obToEntity">DayInSchedule DTO object</param>
        /// <returns>DayInSchedule</returns>
        public static DayInSchedule Convert(IDayInScheduleDataDto obToEntity)
        {
            return new DayInSchedule
            {
                Date = obToEntity.Date,
                Id = obToEntity.Id,
                IsHoliday = obToEntity.IsHoliday,
            };
        }

        #endregion
    }
}
