﻿using System;
using System.Collections.Generic;

namespace CourseManSystem.DAL.Contracts.Dtos
{
    public interface ICourseDataDto
    {
        Guid Id { get; set; }
        string CourseTitle { get; set; }
        string Description { get; set; }
        string CreatorName { get; set; }
        bool IsFinished { get; set; }
        DateTime DateOfCreation { get; set; }
        DateTime DateOfUpdating { get; set; }
        ICollection<ITopicDataDto> Topics { get; set; }
        ICollection<IUserDataDto> Members { get; set; }
    }
}
