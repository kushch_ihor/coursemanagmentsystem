﻿using System;

namespace CourseManSystem.DAL.Contracts.Dtos
{
    public interface ITopicDataDto
    {
        Guid Id { get; set; }
        string Title { get; set; }
        string Description { get; set; }
        Guid CourseId { get; set; }
        DateTime Day { get; set; }
    }
}
