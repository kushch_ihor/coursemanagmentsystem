﻿using System;

namespace CourseManSystem.DAL.Contracts.Dtos
{
    public interface INotificationDataDto
    {
        Guid Id { get; set; }
        string Title { get; set; }
        string Description { get; set; }
        bool IsViewed { get; set; }
        Guid UserId { get; set; }
    }
}
