﻿using System;

namespace CourseManSystem.DAL.Contracts.Dtos
{
    public interface IDayInScheduleDataDto
    {
        Guid Id { get; set; }
        DateTime Date { get; set; }
        bool IsHoliday { get; set; }
    }
}
