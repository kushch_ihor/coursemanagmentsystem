﻿using System;
using System.Collections.Generic;

namespace CourseManSystem.DAL.Contracts.Dtos
{
    public interface IUserDataDto
    {
        Guid Id { get; set; }
        string Email { get; set; }
        string Password { get; set; }
        string UserName { get; set; }
        string Name { get; set; }
        string Address { get; set; }
        string Role { get; set; }
        ICollection<ICourseDataDto> Courses { get; set; }
        ICollection<INotificationDataDto> Notifications { get; set; }
    }
}
