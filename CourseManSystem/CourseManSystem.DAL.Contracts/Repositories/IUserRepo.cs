﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using CourseManSystem.DAL.Contracts.Dtos;

namespace CourseManSystem.DAL.Contracts.Repositories
{
    public interface IUserRepo
    {
        bool Create(IUserDataDto objToCreate);
        bool Delete(Guid idObjectToDel);
        bool Update(IUserDataDto objToUpdate);
        bool AssignCourseToUser(Guid userId, Guid courseId);
        IUserDataDto GetOneByFunc(Func<IUserDataDto, bool> predicate);
        ICollection<IUserDataDto> GetAll();
        ICollection<IUserDataDto> GetAllByFunc(Func<IUserDataDto, bool> predicate);
    }
}
