﻿using System;
using System.Collections.Generic;
using CourseManSystem.DAL.Contracts.Dtos;

namespace CourseManSystem.DAL.Contracts.Repositories
{
    public interface ICourseRepo
    {
        Guid? Create(ICourseDataDto objToCreate);
        bool Delete(Guid idObjectToDel);
        bool Update(ICourseDataDto objToUpdate);
        bool AddUserToCourse(Guid courseId, Guid userId);
        bool RemoveUserFromCourse(Guid courseId, Guid userId);
        ICourseDataDto GetOneByFunc(Func<ICourseDataDto, bool> predicate);
        ICollection<ICourseDataDto> GetAll();
        ICollection<ICourseDataDto> GetAllByFunc(Func<ICourseDataDto, bool> predicate);
    }
}
