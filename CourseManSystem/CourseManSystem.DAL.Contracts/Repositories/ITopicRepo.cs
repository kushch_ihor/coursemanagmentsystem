﻿using System;
using System.Collections.Generic;
using CourseManSystem.DAL.Contracts.Dtos;

namespace CourseManSystem.DAL.Contracts.Repositories
{
    public interface ITopicRepo
    {
        bool Create(ITopicDataDto objToCreate);
        bool Delete(Guid idObjextToDel);
        bool Update(ITopicDataDto objToUpdate);
        ITopicDataDto GetOneByFunc(Func<ITopicDataDto, bool> predicate);
        ICollection<ITopicDataDto> GetAll();
        ICollection<ITopicDataDto> GetAllByFunc(Func<ITopicDataDto, bool> predicate);
    }
}
