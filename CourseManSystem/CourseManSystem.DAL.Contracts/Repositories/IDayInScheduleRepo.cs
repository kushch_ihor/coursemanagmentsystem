﻿using System;
using System.Collections.Generic;
using CourseManSystem.DAL.Contracts.Dtos;

namespace CourseManSystem.DAL.Contracts.Repositories
{
    public interface IDayInScheduleRepo
    {
        bool Create(IDayInScheduleDataDto objToCreate);
        bool Delete(Guid idObjectToDel);
        bool Update(IDayInScheduleDataDto objToUpdate);
        IDayInScheduleDataDto GetOneByFunc(Func<IDayInScheduleDataDto, bool> predicate);
        ICollection<IDayInScheduleDataDto> GetAll();
        ICollection<IDayInScheduleDataDto> GetAllByFunc(Func<IDayInScheduleDataDto, bool> predicate);
    }
}
