﻿using System;
using System.Collections.Generic;
using CourseManSystem.DAL.Contracts.Dtos;

namespace CourseManSystem.DAL.Contracts.Repositories
{
    public interface INotificationRepo 
    {
        bool Create(INotificationDataDto objToCreate);
        bool Delete(Guid idObjectToDel);
        bool Update(INotificationDataDto objToUpdate);
        INotificationDataDto GetOneByFunc(Func<INotificationDataDto, bool> predicate);
        ICollection<INotificationDataDto> GetAll();
        ICollection<INotificationDataDto> GetAllByFunc(Func<INotificationDataDto, bool> predicate);
    }
}
