﻿using CourseManSystem.DAL.Contracts.Repositories;
using CourseManSystem.DAL.Repositories;
using Ninject.Modules;

namespace CourseManSystem.DAL.IoC
{
    public class NinjectBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<INotificationRepo>().To<NotificationRepo>();
            Bind<IUserRepo>().To<UserRepo>();
            Bind<ICourseRepo>().To<CourseRepo>();
            Bind<ITopicRepo>().To<TopicRepo>();
            Bind<IDayInScheduleRepo>().To<DayInScheduleRepo>();
        }
    }
}
