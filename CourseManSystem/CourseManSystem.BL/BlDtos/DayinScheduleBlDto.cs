﻿using System;
using CourseManSystem.DAL.Contracts.Dtos;

namespace CourseManSystem.BL.BlDtos
{
    public class DayinScheduleBlDto : IDayInScheduleDataDto
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public bool IsHoliday { get; set; }
    }
}
