﻿using System;
using CourseManSystem.DAL.Contracts.Dtos;

namespace CourseManSystem.BL.BlDtos
{
    public class TopicBlDto : ITopicDataDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid CourseId { get; set; }
        public DateTime Day { get; set; }
    }
}
