﻿using System;
using System.Collections.Generic;
using CourseManSystem.DAL.Contracts.Dtos;

namespace CourseManSystem.BL.BlDtos
{
    public class CourseBlDto : ICourseDataDto
    {
        public Guid Id { get; set; }
        public string CourseTitle { get; set; }
        public string Description { get; set; }
        public string CreatorName { get; set; }
        public bool IsFinished { get; set; }
        public DateTime DateOfCreation { get; set; }
        public DateTime DateOfUpdating { get; set; }

        public ICollection<ITopicDataDto> Topics { get; set; }
        public ICollection<IUserDataDto> Members { get; set; }

        public CourseBlDto()
        {
            Topics = new List<ITopicDataDto>();
            Members = new List<IUserDataDto>();
        }
    }
}
