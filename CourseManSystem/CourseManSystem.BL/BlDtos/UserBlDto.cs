﻿using System;
using System.Collections.Generic;
using CourseManSystem.DAL.Contracts.Dtos;

namespace CourseManSystem.BL.BlDtos
{
    public class UserBlDto : IUserDataDto
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public ICollection<ICourseDataDto> Courses { get; set; }
        public ICollection<INotificationDataDto> Notifications { get; set; }

        public UserBlDto()
        {
            Courses = new List<ICourseDataDto>();
            Notifications = new List<INotificationDataDto>();
        }
    }
}
