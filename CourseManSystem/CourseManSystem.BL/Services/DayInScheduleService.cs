﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CourseManSystem.BL.BlDtos;
using CourseManSystem.BL.BlModels;
using CourseManSystem.BL.Contracts.Models;
using CourseManSystem.BL.Contracts.Servicees;
using CourseManSystem.DAL.Contracts.Dtos;
using CourseManSystem.DAL.Contracts.Repositories;
using CourseManSystem.DAL.IoC;
using Ninject;

namespace CourseManSystem.BL.Services
{
    /// <summary>
    /// "DayInSchedule" table BL service
    /// </summary>
    public class DayInScheduleService : IDayInScheduleService
    {
        #region Initialize DayInSchadule DAL Repo
        private readonly IKernel _kernel;
        private readonly IDayInScheduleRepo _dayInScheduleRepo;

        public DayInScheduleService()
        {
            _kernel = new StandardKernel(new NinjectBindings());
            _dayInScheduleRepo = _kernel.Get<IDayInScheduleRepo>();
        }
        #endregion

        /// <summary>
        /// Ctreates new DayInSchadule in database by BL DTO
        /// </summary>
        /// <param name="objToCreate"> DayInSchadule BL DTO </param>
        /// <returns>true or false depend on succsesfull operation</returns>
        public bool Create(IDayInScheduleBlModel objToCreate)
        {
            return _dayInScheduleRepo.Create(ModelToDataDto(objToCreate));
        }
        /// <summary>
        /// Deletes DayInSchadule from table by id
        /// </summary>
        /// <param name="idObjectToDel">Guid id of object to delete</param>
        /// <returns>success of executing delete method</returns>
        public bool Delete(Guid idObjectToDel)
        {
            return _dayInScheduleRepo.Delete(idObjectToDel);
        }
        /// <summary>
        /// Updates DayInSchadule in table by DTO BL object
        /// </summary>
        /// <param name="objToUpdate">Model Bl object</param>
        /// <returns>Success of method execution</returns>
        public bool Update(IDayInScheduleBlModel objToUpdate)
        {
            return _dayInScheduleRepo.Update(ModelToDataDto(objToUpdate));
        }
        /// <summary>
        /// Gets one object from database by his id
        /// </summary>
        /// <param name="idObj">Guid id </param>
        /// <returns>DayInSchadule model BL object </returns>
        public IDayInScheduleBlModel GetOneById(Guid idObj)
        {
            return DtoToModel(_dayInScheduleRepo.GetOneByFunc(o => o.Id == idObj));
        }
        /// <summary>
        /// Gets one object from database by date
        /// </summary>
        /// <param name="date">Date in days </param>
        /// <returns>DayInSchadule model BL object </returns>
        public IDayInScheduleBlModel GetOneByDate(DateTime date)
        {
            return DtoToModel(_dayInScheduleRepo.GetOneByFunc(o => o.Date == date));
        }
        /// <summary>
        /// Gets all DayInSchadule objects from database
        /// </summary>
        /// <returns>DayInSchadule objects list</returns>
        public ICollection<IDayInScheduleBlModel> GetAll()
        {
            return _dayInScheduleRepo.GetAll().Select(o => DtoToModel(o)).ToList();
        }
        /// <summary>
        /// Gets all DayInSchadule objects by some filter
        /// </summary>
        /// <param name="predicate">Some lyamda function like filter</param>
        /// <returns>Filtred DayInSchadule objects list</returns>
        public ICollection<IDayInScheduleBlModel> GetAllByFunc(Func<IDayInScheduleBlModel, bool> predicate)
        {
            var map = Mapper.Map<Func<IDayInScheduleDataDto, bool>>(predicate);
            return _dayInScheduleRepo.GetAllByFunc(map).Select(o => DtoToModel(o)).ToList();
        }

        #region MappingDtos
        private IDayInScheduleDataDto ModelToDataDto(IDayInScheduleBlModel objToDto)
        {
            return new DayinScheduleBlDto
            {
                Date = objToDto.Date,
                Id = objToDto.Id,
                IsHoliday = objToDto.IsHoliday
            };
        }

        private IDayInScheduleBlModel DtoToModel(IDayInScheduleDataDto objToModel)
        {
            return new DayInSchaduleBlModel
            {
                Date = objToModel.Date,
                Id = objToModel.Id,
                IsHoliday = objToModel.IsHoliday
            };
        }
        #endregion

    }
}
