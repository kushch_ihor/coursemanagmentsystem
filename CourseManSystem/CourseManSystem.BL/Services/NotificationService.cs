﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CourseManSystem.BL.BlModels;
using CourseManSystem.BL.Contracts.Models;
using CourseManSystem.BL.Contracts.Servicees;
using CourseManSystem.DAL.Contracts.Dtos;
using CourseManSystem.DAL.Contracts.Repositories;
using CourseManSystem.DAL.DataDtos;
using CourseManSystem.DAL.IoC;
using Ninject;

namespace CourseManSystem.BL.Services
{
    /// <summary>
    /// "Notification" table BL service
    /// </summary>
    public class NotificationService : INotificationService
    {
        #region Initialize Notification DAL Repo
        private readonly IKernel _kernel;
        private readonly INotificationRepo _notificationRepo;

        public NotificationService()
        {
            _kernel = new StandardKernel(new NinjectBindings());
            _notificationRepo = _kernel.Get<INotificationRepo>();
        }
        #endregion

        /// <summary>
        /// Creates new Notification in database by BL DTO
        /// </summary>
        /// <param name="objToCreate"> DayInSchadule BL DTO </param>
        /// <returns>true or false depend on succsesfull operation</returns>
        public bool Create(INotificationBlModel objToCreate)
        {
            return _notificationRepo.Create(ModelToDto(objToCreate));
        }
        /// <summary>
        /// Deletes notification from table by id
        /// </summary>
        /// <param name="idObjectToDel">Guid id of object to delete</param>
        /// <returns>success of executing delete method</returns>
        public bool Delete(Guid idObjectToDel)
        {
            return _notificationRepo.Delete(idObjectToDel);
        }
        /// <summary>
        /// Updates Notification in table by DTO BL object
        /// </summary>
        /// <param name="objToUpdate">Model Bl object</param>
        /// <returns>Success of method execution</returns>
        public bool Update(INotificationBlModel objToUpdate)
        {
            return _notificationRepo.Update(ModelToDto(objToUpdate));
        }
        /// <summary>
        /// Gets one object from database by his id
        /// </summary>
        /// <param name="idObj">Guid id</param>
        /// <returns>Notification model BL object </returns>
        public INotificationBlModel GetOneById(Guid idObj)
        {
            return DtoToModel(_notificationRepo.GetOneByFunc(o => o.Id == idObj));
        }
        /// <summary>
        /// Gets one object from database by title
        /// </summary>
        /// <param name="notificationTitle">Notification's title</param>
        /// <returns>Notification model BL object </returns>
        public INotificationBlModel GetOneByTitle(string notificationTitle)
        {
            return DtoToModel(_notificationRepo.GetOneByFunc(o => o.Title == notificationTitle));
        }
        /// <summary>
        /// Gets all Notification objects from database
        /// </summary>
        /// <returns>Notification objects list</returns>
        public ICollection<INotificationBlModel> GetAll()
        {
            return _notificationRepo.GetAll().Select(o => DtoToModel(o)).ToList();
        }
        /// <summary>
        /// Gets all Notification objects by some filter
        /// </summary>
        /// <param name="predicate">Some lyamda function like filter</param>
        /// <returns>Filtred Notification objects list</returns>
        public ICollection<INotificationBlModel> GetAllByFunc(Func<INotificationBlModel, bool> predicate)
        {
             return _notificationRepo.GetAll().ToList().Select(o => DtoToModel(o)).Where(predicate).ToList();
        }

        #region MappingDtos
        private INotificationBlModel DtoToModel(INotificationDataDto objToModel)
        {
            return new NotificationBlModel
            {
                Description = objToModel.Description,
                Id = objToModel.Id,
                Title = objToModel.Title,
                IsViewed = objToModel.IsViewed,
                UserId = objToModel.UserId
            };
        }

        private INotificationDataDto ModelToDto(INotificationBlModel objToModel)
        {
            return new NotificationDataDto
            {
                Description = objToModel.Description,
                Id = objToModel.Id,
                Title = objToModel.Title,
                IsViewed = objToModel.IsViewed,
                UserId = objToModel.UserId
            };
        }
        #endregion

    }
}
