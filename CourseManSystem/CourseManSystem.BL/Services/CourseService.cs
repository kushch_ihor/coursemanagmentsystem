﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CourseManSystem.BL.BlDtos;
using CourseManSystem.BL.BlModels;
using CourseManSystem.BL.Contracts.Models;
using CourseManSystem.BL.Contracts.Servicees;
using CourseManSystem.DAL.Contracts.Dtos;
using CourseManSystem.DAL.Contracts.Repositories;
using CourseManSystem.DAL.DataDtos;
using CourseManSystem.DAL.IoC;
using Ninject;

namespace CourseManSystem.BL.Services
{
    /// <summary>
    /// "Course" BL service
    /// </summary>
    public class CourseService : ICourseService
    {
        #region Initialize Cource DAL Repo
        private readonly ICourseRepo _courseRepo;
        private readonly IKernel _kernel;
        public CourseService()
        {
            _kernel = new StandardKernel(new NinjectBindings());
            _courseRepo = _kernel.Get<ICourseRepo>();
        }
        #endregion

        /// <summary>
        /// Ctreates new user in database by BL DTO
        /// </summary>
        /// <param name="objToCreate"> Cource BL DTO </param>
        /// <returns>true or false depend on succsesfull operation</returns>
        public Guid? Create(ICourseBlModel objToCreate)
        {
            return _courseRepo.Create(ModelToDataDto(objToCreate));
        }
        /// <summary>
        /// Deletes Cource from table by id
        /// </summary>
        /// <param name="idObjectToDel">Guid id of object to delete</param>
        /// <returns>success of executing of delete method</returns>
        public bool Delete(Guid idObjectToDel)
        {
            return _courseRepo.Delete(idObjectToDel);
        }
        /// <summary>
        /// Updates Course in table by DTO BL object
        /// </summary>
        /// <param name="objToUpdate">Model Bl object </param>
        /// <returns>Success of method execution</returns>
        public bool Update(ICourseBlModel objToUpdate)
        {
            return _courseRepo.Update(ModelToDataDto(objToUpdate));
        }
        /// <summary>
        /// Subscribes user to course
        /// </summary>
        /// <param name="courseId">id of course: Guid</param>
        /// <param name="userId">id of user: Guid</param>
        /// <returns>successed of operation</returns>
        public bool AddUserToCourse(Guid courseId, Guid userId)
        {
            return _courseRepo.AddUserToCourse(courseId, userId);
        }
        /// <summary>
        /// unsubscribes user from course
        /// </summary>
        /// <param name="courseId">id of course: Guid</param>
        /// <param name="userId">id of user: Guid</param>
        /// <returns>successed of operation</returns>
        public bool RemoveUserFromCourse(Guid courseId, Guid userId)
        {
            return _courseRepo.RemoveUserFromCourse(courseId, userId);
        }

        /// <summary>
        /// Gets one object from database by his id
        /// </summary>
        /// <param name="idObj">Guid id </param>
        /// <returns>Course model BL object </returns>
        public ICourseBlModel GetOneById(Guid idObj)
        {
            return DataDtoToModel(_courseRepo.GetOneByFunc(o => o.Id == idObj));
        }
        /// <summary>
        /// Gets one object from database by his id
        /// </summary>
        /// <param name="courseTitle">Cource's title </param>
        /// <returns>Cource model BL object </returns>
        public ICourseBlModel GetOneByTitle(string courseTitle)
        {
            return DataDtoToModel(_courseRepo.GetOneByFunc(o => o.CourseTitle == courseTitle));
        }
        /// <summary>
        /// Gets all Course objects from database
        /// </summary>
        /// <returns>Cource objects list</returns>
        public ICollection<ICourseBlModel> GetAll()
        {
            return _courseRepo.GetAll().Select(o => DataDtoToModel(o)).ToList();
        }
        /// <summary>
        /// Gets all Course objects by some filter
        /// </summary>
        /// <param name="predicate">Some lyamda function like filter</param>
        /// <returns>Filtred Course objects list</returns>
        public ICollection<ICourseBlModel> GetAllByFunc(Func<ICourseBlModel, bool> predicate)
        {
            var map = Mapper.Map<Func<ICourseDataDto, bool>>(predicate);
            return _courseRepo.GetAllByFunc(map).Select(o => DataDtoToModel(o)).ToList();
        }

        #region MappingDtos
        private ICourseDataDto ModelToDataDto(ICourseBlModel objToDataDto)
        {
            return new CourseBlDto
            {
                CourseTitle = objToDataDto.CourseTitle,
                CreatorName = objToDataDto.CreatorName,
                DateOfUpdating = objToDataDto.DateOfUpdating,
                Description = objToDataDto.Description,
                Id = objToDataDto.Id,
                IsFinished = objToDataDto.IsFinished,
                DateOfCreation = objToDataDto.DateOfCreation,
                Members = objToDataDto.Members.Select(o => BlDtoToDataDto(o)).ToList(),
                Topics = objToDataDto.Topics.Select(o => BlDtoToDataDto(o)).ToList()
            };
        }

        private ICourseBlModel DataDtoToModel(ICourseDataDto objToBlModel)
        {
            return new CourseBlModel
            {
                CourseTitle = objToBlModel.CourseTitle,
                CreatorName = objToBlModel.CreatorName,
                DateOfUpdating = objToBlModel.DateOfUpdating,
                DateOfCreation = objToBlModel.DateOfCreation,
                Description = objToBlModel.Description,
                Id = objToBlModel.Id,
                IsFinished = objToBlModel.IsFinished,
                Members = objToBlModel.Members.ToList().Select(o => BlDtoToDataDto(o)).ToList(),
                Topics = objToBlModel.Topics.ToList().Select(o => BlDtoToDataDto(o)).ToList()
            };
        }

        private ITopicDataDto BlDtoToDataDto(ITopicBlModel objectToTransform)
        {
            return new TopicDataDto
            {
                Id = objectToTransform.Id,
                Description = objectToTransform.Description,
                Title = objectToTransform.Title,
                CourseId = objectToTransform.CourseId
            };
        }

        private IUserDataDto BlDtoToDataDto(IUserBlModel objectToTransform)
        {
            return new UserDataDto
            {
                Id = objectToTransform.Id,
            };
        }

        private ITopicBlModel BlDtoToDataDto(ITopicDataDto objectToTransform)
        {
            return new TopicBlModel
            {
                Id = objectToTransform.Id,
                Description = objectToTransform.Description,
                Title = objectToTransform.Title,
                CourseId = objectToTransform.CourseId,
                Day = objectToTransform.Day
            };
        }

        private IUserBlModel BlDtoToDataDto(IUserDataDto objectToTransform)
        {
            return new UserBlModel
            {
                Id = objectToTransform.Id
            };
        }
        #endregion

    }
}
