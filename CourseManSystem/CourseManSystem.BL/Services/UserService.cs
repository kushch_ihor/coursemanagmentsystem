﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CourseManSystem.BL.BlModels;
using CourseManSystem.BL.Contracts.Models;
using CourseManSystem.BL.Contracts.Servicees;
using CourseManSystem.DAL.Contracts.Dtos;
using CourseManSystem.DAL.Contracts.Repositories;
using CourseManSystem.DAL.DataDtos;
using CourseManSystem.DAL.IoC;
using Ninject;

namespace CourseManSystem.BL.Services
{
    /// <summary>
    /// "UserProfile" table BL service
    /// </summary>
    public class UserService : IUserService
    {
        #region InitializeuserDataRepo
        private readonly IKernel _kernel;
        private readonly IUserRepo _userRepo;

        public UserService()
        {
            _kernel = new StandardKernel(new NinjectBindings());
            _userRepo = _kernel.Get<IUserRepo>();
        }
        #endregion

        /// <summary>
        /// Creates new UserProfile in database by BL DTO
        /// </summary>
        /// <param name="objToCreate"> UserProfile BL DTO </param>
        /// <returns>true or false depend on succsesfull operation</returns>
        public bool  Create(IUserBlModel objToCreate)
        {
            return  _userRepo.Create(ModelToDto(objToCreate));
        }

        /// <summary>
        /// Deletes user from table by id
        /// </summary>
        /// <param name="idObjectToDel">Guid id of object to delete</param>
        /// <returns>success of executing delete method</returns>
        public bool Delete(Guid idObjectToDel)
        {
            return _userRepo.Delete(idObjectToDel);
        }
        /// <summary>
        /// Updates UserProfile in table by DTO BL object
        /// </summary>
        /// <param name="objToUpdate">Model Bl object</param>
        /// <returns>Success of method execution</returns>
        public bool Update(IUserBlModel objToUpdate)
        {
            return _userRepo.Update(ModelToDto(objToUpdate));
        }

        /// <summary>
        /// Subscribe user to course
        /// </summary>
        /// <param name="userId"> UserProfile Id</param>
        /// <param name="courseId">Course Id</param>
        /// <returns>Successful of assign to course</returns>
        public bool AssignCourseToUser(Guid userId, Guid courseId)
        {
            return _userRepo.AssignCourseToUser(userId, courseId);
        }

        /// <summary>
        /// Gets one object drom database by his id
        /// </summary>
        /// <param name="idObj">Guid id</param>
        /// <returns>UserProfile model BL object </returns>
        public IUserBlModel GetOneById(Guid idObj)
        {
            return DtoToModel(_userRepo.GetOneByFunc(o => o.Id == idObj));
        }
        /// <summary>
        /// Gets one object drom database by his name
        /// </summary>
        /// <param name="userName">Name of user</param>
        /// <returns>UserProfile model BL object </returns>
        public IUserBlModel GetOneByName(string userName)
        {
            return DtoToModel(_userRepo.GetOneByFunc(o => o.UserName == userName));
        }
        /// <summary>
        /// Gets all UserProfile objects from database
        /// </summary>
        /// <returns>UserProfile objects list</returns>
        public ICollection<IUserBlModel> GetAll()
        {
            return _userRepo.GetAll().Select(o => DtoToModel(o)).ToList();
        }
        /// <summary>
        /// Gets all UserProfile objects by some filter
        /// </summary>
        /// <param name="predicate">Some lyamda function like filter</param>
        /// <returns>Filtred user objects list</returns>
        public ICollection<IUserBlModel> GetAllByFunc(Func<IUserBlModel, bool> predicate)
        {
            Mapper.Initialize(o => o.CreateMap<UserBlModel,UserDataDto>());
            var map = Mapper.Map<Func<IUserBlModel, bool>, Func<IUserDataDto, bool>>(predicate);
            return _userRepo.GetAllByFunc(map).Select(o => DtoToModel(o)).ToList();
        }

        #region MappingDtos
        private IUserBlModel DtoToModel(IUserDataDto objToModel)
        {
            return new UserBlModel
            {
                Email = objToModel.Email,
                Id = objToModel.Id,
                Password = objToModel.Password,
                Role = objToModel.Role,
                UserName = objToModel.UserName,
                Courses = DtoListToModelList(objToModel.Courses)
            };
        }
        private IUserDataDto ModelToDto(IUserBlModel objToModel)
        {
            return new UserDataDto
            {
                Email = objToModel.Email,
                //Id = objToModel.Id,
                Password = objToModel.Password,
                //Role = objToModel.Role,
                UserName = objToModel.UserName
            };
        }

        private ICollection<ICourseBlModel> DtoListToModelList(ICollection<ICourseDataDto> courseList)
        {
            ICollection<ICourseBlModel> courses = new List<ICourseBlModel>();
            foreach (var course in courseList)
            {
                courses.Add(new CourseBlModel
                {
                    Id = course.Id,
                    CreatorName = course.CreatorName,
                    CourseTitle = course.CourseTitle,
                    Description = course.Description,
                    DateOfCreation = course.DateOfCreation,
                    DateOfUpdating = course.DateOfUpdating,
                    IsFinished = course.IsFinished,
                    Topics = course.Topics.Select(DtoToModel).ToList()
                });
            }
            return courses;
        }

        private ITopicBlModel DtoToModel(ITopicDataDto topicDto)
        {
            return new TopicBlModel
            {
                Id = topicDto.Id,
                Day = topicDto.Day,
                Title = topicDto.Title,
                Description = topicDto.Description
            };
        }
        #endregion

    }
}
