﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CourseManSystem.BL.BlModels;
using CourseManSystem.BL.Contracts.Models;
using CourseManSystem.BL.Contracts.Servicees;
using CourseManSystem.DAL.Contracts.Dtos;
using CourseManSystem.DAL.Contracts.Repositories;
using CourseManSystem.DAL.DataDtos;
using CourseManSystem.DAL.IoC;
using Ninject;

namespace CourseManSystem.BL.Services
{
    /// <summary>
    /// "Topic" table BL service
    /// </summary>
    public class TopicService : ITopicService
    {
        #region Initialize Topic DAL Repo
        private readonly IKernel _kernel;
        private readonly ITopicRepo _topicRepo;

        public TopicService()
        {
            _kernel = new StandardKernel(new NinjectBindings());
            _topicRepo = _kernel.Get<ITopicRepo>();
        }
        #endregion

        /// <summary>
        /// Creates new Topic in database by BL DTO
        /// </summary>
        /// <param name="objToCreate"> Topic BL DTO </param>
        /// <returns>true or false depend on succsesfull operation</returns>
        public bool Create(ITopicBlModel objToCreate)
        {
            return _topicRepo.Create(ModelToDto(objToCreate));
        }
        /// <summary>
        /// Deletes Topic from table by id
        /// </summary>
        /// <param name="idObjectToDel">Guid id of object to delete</param>
        /// <returns>success of executing delete method</returns>
        public bool Delete(Guid idObjectToDel)
        {
            return _topicRepo.Delete(idObjectToDel);
        }
        /// <summary>
        /// Updates Topic in table by DTO BL object
        /// </summary>
        /// <param name="objToUpdate">Model Bl object</param>
        /// <returns>Success of method execution</returns>
        public bool Update(ITopicBlModel objToUpdate)
        {
            return _topicRepo.Update(ModelToDto(objToUpdate));
        }
        /// <summary>
        /// Gets one object from database by his id
        /// </summary>
        /// <param name="idObj">Guid id</param>
        /// <returns>Topic model BL object </returns>
        public ITopicBlModel GetOneById(Guid idObj)
        {
            return DtoToModel(_topicRepo.GetOneByFunc(o => o.Id == idObj));
        }
        /// <summary>
        /// Gets one object from database by his title
        /// </summary>
        /// <param name="topicTitle">Topic's title</param>
        /// <returns>Topic model BL object </returns>
        public ITopicBlModel GetOneByTitle(string topicTitle)
        {
            return DtoToModel(_topicRepo.GetOneByFunc(o => o.Title == topicTitle));
        }
        /// <summary>
        /// Gets all Topic objects from database
        /// </summary>
        /// <returns>Topic objects list</returns>
        public ICollection<ITopicBlModel> GetAll()
        {
            return _topicRepo.GetAll().Select(o => DtoToModel(o)).ToList();
        }
        /// <summary>
        /// Gets all Topic objects by some filter
        /// </summary>
        /// <param name="predicate">Some lyamda function like filter</param>
        /// <returns>Filtred Topic objects list</returns>
        public ICollection<ITopicBlModel> GetAllByFunc(Func<ITopicBlModel, bool> predicate)
        {
            var map = Mapper.Map<Func<ITopicDataDto, bool>>(predicate);
            return _topicRepo.GetAllByFunc(map).Select(o => DtoToModel(o)).ToList();
        }

        #region MappingDtos
        private ITopicBlModel DtoToModel(ITopicDataDto objToModel)
        {
            return new TopicBlModel
            {
                Id = objToModel.Id,
                Description = objToModel.Description,
                Title = objToModel.Title,
                CourseId = objToModel.CourseId,
                Day = objToModel.Day
            };
        }
        private ITopicDataDto ModelToDto(ITopicBlModel objToModel)
        {
            return new TopicDataDto
            {
                Id = objToModel.Id,
                Description = objToModel.Description,
                Title = objToModel.Title,
                CourseId = objToModel.CourseId,
                Day = objToModel.Day
            };
        }
        #endregion

    }
}
