﻿using CourseManSystem.BL.Contracts.Models;
using System;

namespace CourseManSystem.BL.BlModels
{
    public class DayInSchaduleBlModel : IDayInScheduleBlModel
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public bool IsHoliday { get; set; }
    }
}
