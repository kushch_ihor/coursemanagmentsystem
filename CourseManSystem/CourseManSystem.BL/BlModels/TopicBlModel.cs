﻿using CourseManSystem.BL.Contracts.Models;
using System;

namespace CourseManSystem.BL.BlModels
{
    public class TopicBlModel : ITopicBlModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid CourseId { get; set; }
        public DateTime Day { get; set; }
    }
}
