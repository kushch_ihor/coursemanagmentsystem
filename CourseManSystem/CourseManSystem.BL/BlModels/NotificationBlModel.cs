﻿using CourseManSystem.BL.Contracts.Models;
using System;

namespace CourseManSystem.BL.BlModels
{
    public class NotificationBlModel : INotificationBlModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsViewed { get; set; }
        public Guid UserId { get; set; }
    }
}
