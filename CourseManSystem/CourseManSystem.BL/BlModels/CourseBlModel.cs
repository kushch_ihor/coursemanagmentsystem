﻿using System;
using System.Collections.Generic;
using CourseManSystem.BL.Contracts.Models;

namespace CourseManSystem.BL.BlModels
{
    public class CourseBlModel : ICourseBlModel
    {
        public Guid Id { get; set; }
        public string CourseTitle { get; set; }
        public string Description { get; set; }
        public string CreatorName { get; set; }
        public bool IsFinished { get; set; }
        public DateTime DateOfCreation { get; set; }
        public DateTime DateOfUpdating { get; set; }

        public List<ITopicBlModel> Topics { get; set; }
        public List<IUserBlModel> Members { get; set; }

        public CourseBlModel()
        {
            Topics = new List<ITopicBlModel>();
            Members = new List<IUserBlModel>();
        }
    }
}
