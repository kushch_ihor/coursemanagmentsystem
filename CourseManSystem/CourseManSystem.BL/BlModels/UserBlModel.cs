﻿using CourseManSystem.BL.Contracts.Models;
using System;
using System.Collections.Generic;

namespace CourseManSystem.BL.BlModels
{
    public class UserBlModel : IUserBlModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public ICollection<ICourseBlModel> Courses { get; set; }
        public ICollection<INotificationBlModel> Notifications { get; set; }
    }
}
