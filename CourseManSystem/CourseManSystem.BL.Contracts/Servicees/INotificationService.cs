﻿using System;
using System.Collections.Generic;
using CourseManSystem.BL.Contracts.Models;

namespace CourseManSystem.BL.Contracts.Servicees
{
    public interface INotificationService
    {
        bool Create(INotificationBlModel objToCreate);
        bool Delete(Guid idObjectToDel);
        bool Update(INotificationBlModel objToUpdate);
        INotificationBlModel GetOneById(Guid idObj);
        INotificationBlModel GetOneByTitle(string notificationTitle);
        ICollection<INotificationBlModel> GetAll();
        ICollection<INotificationBlModel> GetAllByFunc(Func<INotificationBlModel, bool> predicate);
    }
}
