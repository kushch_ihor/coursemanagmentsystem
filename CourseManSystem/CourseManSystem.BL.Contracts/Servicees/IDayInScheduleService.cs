﻿using System;
using System.Collections.Generic;
using CourseManSystem.BL.Contracts.Models;

namespace CourseManSystem.BL.Contracts.Servicees
{
    public interface IDayInScheduleService
    {
        bool Create(IDayInScheduleBlModel objToCreate);
        bool Delete(Guid idObjectToDel);
        bool Update(IDayInScheduleBlModel objToUpdate);
        IDayInScheduleBlModel GetOneById(Guid idObj);
        IDayInScheduleBlModel GetOneByDate(DateTime date);
        ICollection<IDayInScheduleBlModel> GetAll();
        ICollection<IDayInScheduleBlModel> GetAllByFunc(Func<IDayInScheduleBlModel, bool> predicate);
    }
}
