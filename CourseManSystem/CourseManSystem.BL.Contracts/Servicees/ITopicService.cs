﻿using System;
using System.Collections.Generic;
using CourseManSystem.BL.Contracts.Models;

namespace CourseManSystem.BL.Contracts.Servicees
{
    public interface ITopicService
    {
        bool Create(ITopicBlModel objToCreate);
        bool Delete(Guid idObjectToDel);
        bool Update(ITopicBlModel objToUpdate);
        ITopicBlModel GetOneById(Guid idObj);
        ITopicBlModel GetOneByTitle(string topicTitle);
        ICollection<ITopicBlModel> GetAll();
        ICollection<ITopicBlModel> GetAllByFunc(Func<ITopicBlModel, bool> predicate);
    }
}
