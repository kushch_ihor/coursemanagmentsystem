﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CourseManSystem.BL.Contracts.Models;

namespace CourseManSystem.BL.Contracts.Servicees
{
    public interface IUserService
    {
        bool Create(IUserBlModel objToCreate);
        bool Delete(Guid idObjectToDel);
        bool Update(IUserBlModel objToUpdate);
        bool AssignCourseToUser(Guid userId, Guid courseId);
        IUserBlModel GetOneById(Guid idObj);
        IUserBlModel GetOneByName(string userName);
        ICollection<IUserBlModel> GetAll();
        ICollection<IUserBlModel> GetAllByFunc(Func<IUserBlModel, bool> predicate);
    }
}
