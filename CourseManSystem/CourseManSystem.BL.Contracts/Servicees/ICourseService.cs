﻿using System;
using System.Collections.Generic;
using CourseManSystem.BL.Contracts.Models;

namespace CourseManSystem.BL.Contracts.Servicees
{
    public interface ICourseService
    {
        Guid? Create(ICourseBlModel objToCreate);
        bool Delete(Guid idObjectToDel);
        bool Update(ICourseBlModel objToUpdate);
        bool AddUserToCourse(Guid courseId, Guid userId);
        bool RemoveUserFromCourse(Guid courseId, Guid userId);
        ICourseBlModel GetOneById(Guid idObj);
        ICourseBlModel GetOneByTitle(string courseTitle);
        ICollection<ICourseBlModel> GetAll();
        ICollection<ICourseBlModel> GetAllByFunc(Func<ICourseBlModel, bool> predicate);
    }
}
