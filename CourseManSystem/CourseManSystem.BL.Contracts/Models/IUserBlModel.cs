﻿using System;
using System.Collections.Generic;


namespace CourseManSystem.BL.Contracts.Models
{
    public interface IUserBlModel
    {
        Guid Id { get; set; }
        string Email { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string Role { get; set; }
        ICollection<ICourseBlModel> Courses { get; set; }
        ICollection<INotificationBlModel> Notifications { get; set; }
    }
}
