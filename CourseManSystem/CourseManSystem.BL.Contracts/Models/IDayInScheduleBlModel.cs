﻿using System;

namespace CourseManSystem.BL.Contracts.Models
{
    public interface IDayInScheduleBlModel
    {
        Guid Id { get; set; }
        DateTime Date { get; set; }
        bool IsHoliday { get; set; }
    }
}
