﻿using System;

namespace CourseManSystem.BL.Contracts.Models
{
    public interface ITopicBlModel
    {
        Guid Id { get; set; }
        string Title { get; set; }
        string Description { get; set; }
        Guid CourseId { get; set; }
        DateTime Day { get; set; }
    }
}
