﻿using System;

namespace CourseManSystem.BL.Contracts.Models
{
    public interface INotificationBlModel
    {
        Guid Id { get; set; }
        string Title { get; set; }
        string Description { get; set; }
        bool IsViewed { get; set; }
        Guid UserId { get; set; }
    }
}
