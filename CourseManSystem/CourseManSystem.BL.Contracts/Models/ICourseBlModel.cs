﻿using System;
using System.Collections.Generic;

namespace CourseManSystem.BL.Contracts.Models
{
    public interface ICourseBlModel
    {
        Guid Id { get; set; }
        string CourseTitle { get; set; }
        string Description { get; set; }
        string CreatorName { get; set; }
        bool IsFinished { get; set; }
        DateTime DateOfCreation { get; set; }
        DateTime DateOfUpdating { get; set; }
        List<ITopicBlModel> Topics { get; set; }
        List<IUserBlModel> Members { get; set; }
    }
}
