﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using CourseManSystem.DAL.Entities.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CourseManSystem.DAL.Entities.IdentityEnteties
{
    public class CourseUser : IdentityUser<Guid,UserLogin, GuidUserRole, UserClaim>, IDisposable
    {
        public CourseUser()
        {
            Id = Guid.NewGuid();
        }

        public CourseUser(string name) : this()
        {
            UserName = name;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<CourseUser,Guid> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

        [Required]
        public virtual UserProfile UserProfile { get; set; }

        public void Dispose()
        {
        }
    }
}
