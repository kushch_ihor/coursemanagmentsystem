﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CourseManSystem.DAL.Entities.IdentityEnteties
{
    public class UserRole : IdentityRole<Guid,GuidUserRole>
    {
        public UserRole()
        {
            Id = Guid.NewGuid();
        }

        public UserRole(string name) : this()
        {
            Name = name;
        }
    }
}
