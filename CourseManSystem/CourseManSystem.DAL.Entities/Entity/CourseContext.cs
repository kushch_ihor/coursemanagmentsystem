﻿using System;
using System.Data.Entity;
using CourseManSystem.DAL.Entities.IdentityEnteties;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CourseManSystem.DAL.Entities.Entity
{
    public class CourseContext : IdentityDbContext<CourseUser,UserRole,Guid,UserLogin,GuidUserRole,UserClaim>
    {
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<DayInSchedule> DaysInSchedule { get; set; }

        public CourseContext() : base(Constants.Constants.ConnectionString)
        {
            Database.SetInitializer(new IdentityCourseDbInitializer());
        }

        public static CourseContext Create()
        {
            return new CourseContext();
        }
    }
}
