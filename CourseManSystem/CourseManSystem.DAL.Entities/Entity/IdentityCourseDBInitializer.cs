﻿using System;
using System.Data.Entity;
using System.Linq;
using CourseManSystem.DAL.Entities.IdentityEnteties;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CourseManSystem.DAL.Entities.Entity
{
    public class IdentityCourseDbInitializer : CreateDatabaseIfNotExists<CourseContext>
    {
        protected override void Seed(CourseContext context)
        {
            base.Seed(context);

            if (!context.Roles.Any(r => r.Name == "admin"))
            {
                var store = new RoleStore<UserRole, Guid, GuidUserRole>(context);
                var manager = new RoleManager<UserRole, Guid>(store);
                foreach (var item in Constants.Constants.RolesList)
                {
                    var role = new UserRole(item);
                    manager.Create(role);
                }

            }

            if (!context.Users.Any(u => u.UserName == "admin"))
            {
                var store = new UserStore<CourseUser, UserRole, Guid, UserLogin, GuidUserRole, UserClaim>(context);
                var manager = new UserManager<CourseUser, Guid>(store);
                var user = new CourseUser
                {
                    UserName = "admin",
                    Email = "admin@mail.com",
                    UserProfile = new UserProfile()
                };

                manager.Create(user, "adminadmin");
                manager.AddToRole(user.Id, "admin");
            }

        }
    }
}
