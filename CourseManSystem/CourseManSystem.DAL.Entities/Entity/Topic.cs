﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CourseManSystem.DAL.Entities.Entity
{
    [Table("Topic")]
    public class Topic : BaseId
    {
        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public Guid CourseId { get; set; }

        [ForeignKey("CourseId")]
        public Course Course { get; set; }

        public virtual ICollection<DayInSchedule> DaysInSchedule { get; set; }
    }
}
