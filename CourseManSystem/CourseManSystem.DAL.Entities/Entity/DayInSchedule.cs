﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CourseManSystem.DAL.Entities.Entity
{
    [Table("DayInSchedule")]
    public class DayInSchedule : BaseId
    {
        [Required]
        public DateTime Date { get; set; }

        [Required]
        public bool IsHoliday { get; set; }

        public virtual ICollection<Topic> Topics { get; set; }
    }
}
