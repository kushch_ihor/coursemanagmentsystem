﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CourseManSystem.DAL.Entities.Entity
{
    [Table("Course")]
    public class Course : BaseId
    {
        [Required]
        [StringLength(255)]
        public string CourseTitle { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [Required]
        [StringLength(255)]
        public string CreatorName { get; set; }

        [Required]
        public bool IsFinished { get; set; }

        public DateTime DateOfCreation { get; set; }

        public DateTime DateofUpdating { get; set; }

        public virtual ICollection<Topic> Topics { get; set; }
        
        public virtual ICollection<UserProfile> UserProfiles { get; set; }

    }
}
