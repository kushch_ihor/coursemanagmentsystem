﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CourseManSystem.DAL.Entities.IdentityEnteties;

namespace CourseManSystem.DAL.Entities.Entity
{
    [Table("UserProfile")]
    public class UserProfile
    {

        [Key]
        [ForeignKey("CourseUser")]
        public Guid Id { get; set; }
        public virtual CourseUser CourseUser { get; set; }

        public virtual ICollection<Notification> Notifications { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
    }
}
