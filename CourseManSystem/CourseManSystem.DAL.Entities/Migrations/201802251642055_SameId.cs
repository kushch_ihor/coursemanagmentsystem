namespace CourseManSystem.DAL.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SameId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserProfileCourses", "UserProfile_Id", "dbo.UserProfile");
            DropForeignKey("dbo.AspNetUsers", "Id", "dbo.UserProfile");
            DropForeignKey("dbo.Notification", "UserProfileId", "dbo.UserProfile");
            DropIndex("dbo.AspNetUsers", new[] { "Id" });
            DropPrimaryKey("dbo.UserProfile");
            AlterColumn("dbo.UserProfile", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.UserProfile", "Id");
            CreateIndex("dbo.UserProfile", "Id");
            AddForeignKey("dbo.UserProfileCourses", "UserProfile_Id", "dbo.UserProfile", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Notification", "UserProfileId", "dbo.UserProfile", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notification", "UserProfileId", "dbo.UserProfile");
            DropForeignKey("dbo.UserProfileCourses", "UserProfile_Id", "dbo.UserProfile");
            DropIndex("dbo.UserProfile", new[] { "Id" });
            DropPrimaryKey("dbo.UserProfile");
            AlterColumn("dbo.UserProfile", "Id", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.UserProfile", "Id");
            CreateIndex("dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Notification", "UserProfileId", "dbo.UserProfile", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUsers", "Id", "dbo.UserProfile", "Id");
            AddForeignKey("dbo.UserProfileCourses", "UserProfile_Id", "dbo.UserProfile", "Id", cascadeDelete: true);
        }
    }
}
