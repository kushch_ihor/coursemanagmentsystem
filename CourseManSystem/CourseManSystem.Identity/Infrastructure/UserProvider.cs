﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CourseManSystem.DAL.Entities.Entity;
using CourseManSystem.DAL.Entities.IdentityEnteties;
using CourseManSystem.Identity.Contracts;
using CourseManSystem.Identity.Dtos;
using CourseManSystem.Identity.Managers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace CourseManSystem.Identity.Infrastructure
{
    public class UserProvider
    {
        public static CourseUserManager GetUserManager()
        {
            return HttpContext.Current.GetOwinContext().GetUserManager<CourseUserManager>();
        }

        public static UserRoleManager GetUserRoleManager()
        {
            return HttpContext.Current.GetOwinContext().Get<UserRoleManager>();
        }

        public static  IdentityResult CreateNewUser(IRegisterModel model, string password)
        {
            var user = new CourseUser {UserName = model.UserName, Email = model.Email, UserProfile = new UserProfile()};
            var result = GetUserManager().Create(user, password);
            if (result.Succeeded)
            {
                GetUserManager().AddToRole(user.Id, model.Role);
            }    
            return result;
        }

        public static async Task<IdentityResult>  CreateNewUser(IRegisterModel model)
        {
            var user = new CourseUser { UserName = model.UserName, Email = model.Email, UserProfile = new UserProfile() };
            var result = await GetUserManager().CreateAsync(user);
            if (result.Succeeded)
            {
                await GetUserManager().AddToRoleAsync(user.Id, model.Role);
            }
            return result;
        }

        public static IAuthenticationManager GetAuthenticationManager => HttpContext.Current.GetOwinContext().Authentication;

        public static async Task<bool> FindUserAsync(string userName,string password)
        {
            CourseUser user = await GetUserManager().FindAsync(userName, password);

            return user != null;
        }

        public static async Task<bool> FindUserAsync(UserLoginInfo externalInfo)
        {
            CourseUser user = await GetUserManager().FindAsync(externalInfo);

            return user != null;
        }

        public static async Task<IRegisterModel> FindUserById(Guid id)
        {
            var foundUser = await GetUserManager().FindByIdAsync(id);
            string currentUserRole = null;
            if (foundUser.Roles.Count >= 1)
            {
                currentUserRole = GetNameOfRole(foundUser.Roles);
            }
            return new IdentityUserDto
            {
                Id = foundUser.Id,
                Email = foundUser.Email,
                PhoneNumber = foundUser.PhoneNumber,
                Role = currentUserRole,
                UserName = foundUser.UserName
            };
        }

        public static async Task<IdentityResult> AddLoginAsync(string userName, UserLoginInfo loginInfo)
        {
            var user = await GetUserManager().FindByNameAsync(userName);

            return await GetUserManager().AddLoginAsync(user.Id, loginInfo);
        }

        public static async Task<List<IRegisterModel>> GetAllUsers()
        {
            var userList = await GetUserManager().Users.ToArrayAsync();
            return new List<IRegisterModel>(userList.Select(o => new IdentityUserDto
            {
                Id = o.Id,
                Email = o.Email,
                PhoneNumber = o.PhoneNumber,
                Role = GetNameOfRole(o.Roles),
                UserName = o.UserName
            }).ToList());
        }

        public static async Task<IdentityResult> ResetUserPassword(Guid userId, string currentPassword, string newPasswword)
        {
            return await GetUserManager().ChangePasswordAsync(userId, currentPassword, newPasswword);
        }

        public static async Task<IdentityResult> UpdateUser(IRegisterModel model)
        {
            var user = await GetUserManager().FindByNameAsync(model.UserName);
            user.UserName = model.UserName;
            user.Email = model.Email;
            if (user.Roles.Count > 1)
            {
                await GetUserManager().RemoveFromRolesAsync(user.Id, Constants.Constants.RolesList.ToArray());
            }
            var currentRoleName = GetNameOfRole(user.Roles);
            if (currentRoleName != null)
            {
                await GetUserManager().RemoveFromRoleAsync(user.Id, currentRoleName);
            }
            
            await GetUserManager().AddToRoleAsync(user.Id, model.Role);
            return await GetUserManager().UpdateAsync(user);
        }

        public static async Task<IdentityResult> DeleteUser(string id)
        {
            var user = await GetUserManager().FindByIdAsync(Guid.Parse(id));
            var logins = user.Logins;
            var userRoles = await GetUserManager().GetRolesAsync(user.Id);
            foreach (var login in logins.ToList())
            {
                await GetUserManager().RemoveLoginAsync(login.UserId,
                    new UserLoginInfo(login.LoginProvider, login.ProviderKey));

            }
            if (userRoles.Count > 0)
            {
                foreach (var userRole in userRoles)
                {
                    await GetUserManager().RemoveFromRoleAsync(user.Id, userRole);
                }
            }
            return await GetUserManager().DeleteAsync(user);
        }

        private static string GetNameOfRole(ICollection<GuidUserRole> roleList)
        {
            if (roleList.Count == 0)
            {
                return null;
            }
            return  GetUserRoleManager().FindByIdAsync(roleList.FirstOrDefault().RoleId).Result.Name;
        }

    }
}
