﻿using CourseManSystem.DAL.Entities.IdentityEnteties;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;
using CourseManSystem.Identity.Managers;

namespace CourseManSystem.Identity.Infrastructure
{
    public class CourseUserValidator : UserValidator<CourseUser,Guid>
    {
        public CourseUserValidator(CourseUserManager manager) : base(manager)
        {
            this.AllowOnlyAlphanumericUserNames = true;
            this.RequireUniqueEmail = true;
        }

        public override async Task<IdentityResult> ValidateAsync(CourseUser user)
        {
            var result = await base.ValidateAsync(user);
            if (!result.Succeeded)
            {
                return new IdentityResult("Your email already exist in database or social username has incorrect characters");                    
            }
            foreach (var emailDomain in Constants.Constants.ForbiddenEmailDomains)
            {
                if (user.Email.ToLower().EndsWith(emailDomain))
                {
                    var errors = result.Errors.ToList();
                    errors.Add($"Email domain {emailDomain} is forbidden");
                    result = new IdentityResult(errors);
                }
            }
            
            return result;
        }
    }
}
