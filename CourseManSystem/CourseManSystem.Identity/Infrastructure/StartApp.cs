﻿using System;
using System.Net.Http;
using System.Runtime.InteropServices;
using CourseManSystem.DAL.Entities.Entity;
using CourseManSystem.DAL.Entities.IdentityEnteties;
using CourseManSystem.Identity.Managers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Owin;

[assembly:OwinStartup(typeof(CourseManSystem.Identity.Infrastructure.StartApp))]
namespace CourseManSystem.Identity.Infrastructure
{

    public class StartApp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            appBuilder.CreatePerOwinContext<CourseContext>(CourseContext.Create);
            appBuilder.CreatePerOwinContext<CourseUserManager>(CourseUserManager.Create);
            appBuilder.CreatePerOwinContext<UserRoleManager>(UserRoleManager.Create);

            appBuilder.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Register"),
            });

            appBuilder.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            appBuilder.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions
            {
                AuthenticationType = "Google",
                ClientId = "355499029503-ua1138lleht6qg0gl0aic3i6e8i8khlk.apps.googleusercontent.com",
                ClientSecret = "vNAyxutmxhoIGDfG1vabkuO6",
                Caption = "Authorization by Google+",
                CallbackPath = new PathString("/ExternalLoginCallback"),
                AuthenticationMode = AuthenticationMode.Passive,
                SignInAsAuthenticationType = appBuilder.GetDefaultSignInAsAuthenticationType(),
                BackchannelTimeout = TimeSpan.FromSeconds(60),
                BackchannelHttpHandler = new HttpClientHandler(),
                BackchannelCertificateValidator = null,
                Provider = new GoogleOAuth2AuthenticationProvider()
            });

            appBuilder.UseFacebookAuthentication(new FacebookAuthenticationOptions
            {
                AuthenticationType = "Facebook",
                AppId = "693291044347744",
                AppSecret = "d8104c9e292fc67bb8002657c6157644",
                Caption = "Authorization by Facebook",
                AuthenticationMode = AuthenticationMode.Passive,
                BackchannelTimeout = TimeSpan.FromSeconds(60),
                BackchannelHttpHandler = new WebRequestHandler(),
                BackchannelCertificateValidator = null,
                Provider = new FacebookAuthenticationProvider()
            });

        }
    }
}
