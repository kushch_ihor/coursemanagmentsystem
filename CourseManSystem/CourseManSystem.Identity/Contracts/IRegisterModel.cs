﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseManSystem.Identity.Contracts
{
    public interface IRegisterModel  
    {
        Guid Id { get; set; }
        string Email { get; set; }
        string UserName { get; set; }
        string PhoneNumber { get; set; }
        string Password { get; set; }
        string ConfirmPassword { get; set; }
        string Role { get; set; }
    }
}
