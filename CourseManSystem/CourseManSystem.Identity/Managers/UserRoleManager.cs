﻿using System;
using CourseManSystem.DAL.Entities.Entity;
using CourseManSystem.DAL.Entities.IdentityEnteties;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace CourseManSystem.Identity.Managers
{
    public class UserRoleManager : RoleManager<UserRole,Guid>
    {
        public UserRoleManager(IRoleStore<UserRole,Guid> store) : base(store)
        {
        }

        public static UserRoleManager Create(IdentityFactoryOptions<UserRoleManager> options, IOwinContext context)
        {
            return new UserRoleManager(new RoleStore<UserRole,Guid,GuidUserRole>(context.Get<CourseContext>()));
        }
    }
}
