﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using CourseManSystem.DAL.Entities.Entity;
using CourseManSystem.DAL.Entities.IdentityEnteties;
using CourseManSystem.Identity.Contracts;
using CourseManSystem.Identity.Infrastructure;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace CourseManSystem.Identity.Managers
{
    public class CourseUserManager : UserManager<CourseUser,Guid>
    {

        public CourseUserManager(IUserStore<CourseUser,Guid> store) : base(store)
        {
        }

        public static CourseUserManager Create(IdentityFactoryOptions<CourseUserManager> options, IOwinContext context)
        {
            CourseContext db = context.Get<CourseContext>();
            CourseUserManager manager = new CourseUserManager(new UserStore<CourseUser,UserRole,Guid,UserLogin, GuidUserRole, UserClaim>(db));
            manager.UserValidator = new CourseUserValidator(manager);

            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireDigit = false,
                RequireLowercase = true,
                RequireUppercase = true,
                RequireNonLetterOrDigit = false
            };
            return manager;
        }

        public async Task<ClaimsIdentity> CreateIdentityForLoginModelAsync (ILoginModel model)
        {
            var user = await UserProvider.GetUserManager().FindAsync(model.UserName, model.Password);

            return await UserProvider.GetUserManager()
                .CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        }

        public async Task<ClaimsIdentity> CtreateIdentityForExternalServise (UserLoginInfo userLoginInfo)
        {
            var user = await UserProvider.GetUserManager().FindAsync(userLoginInfo);
            return await UserProvider.GetUserManager()
                .CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        }
    }
}
