﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseManSystem.Constants
{
    public static class Constants
    {
        /// <summary>
        /// Use for shoose nessesary role in the each project
        /// </summary>
        public static List<string> RolesList = new List<string>
        {
            "admin",
            "listener",
            "trainer"
        };
        /// <summary>
        /// Count of items in the list pages like course list or user list pages
        /// </summary>
        public static int ItemsPerPage = 10;
        /// <summary>
        /// Name of current connection string which connects to database
        /// </summary>
        public static string ConnectionString = "CourseContext";
        /// <summary>
        /// List of email domains which users can'n use
        /// </summary>
        public static List<string> ForbiddenEmailDomains = new List<string>
        {
            "@mail.ru",
            "@rambler.ru"
        };
    }
   
}
